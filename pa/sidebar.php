<?php
if (eregi("sidebar.php", $_SERVER['PHP_SELF'])) {
    die("<h4>You don't have right permission to access this file directly.</h4>");
} else {
?>

<!--sidebar start-->
<aside>
  <div id="sidebar"  class="nav-collapse ">
    <!-- sidebar menu start-->
    <ul class="sidebar-menu" id="nav-accordion">
    <br>
      <h5 class="centered">Zalogowany jako: <br><?php echo $_SESSION['user']['username']; ?></h5>
      <li class="mt">
        <a href="index.php">
        <i class="fa fa-th"></i>
        <span>Zestawienie</span>
        </a>
      </li>
      <li>
        <a href="sprzet.php">
        <i class="fa fa-th"></i>
        <span>Magazyn sprzetu</span>
        </a>
        </li>
      <li>
        <a href="rezerwacje.php">
        <i class="fa fa-th"></i>
        <span>Rezerwacje</span>
        </a>
      </li>
<!--       <li>
        <a href="zamowienia.php">
        <i class="fa fa-th"></i>
        <span>Zamowienia</span>
        </a>
      </li> -->
      <li>
        <a href="klienci.php">
        <i class="fa fa-th"></i>
        <span>Klienci</span>
        </a>
      </li>
<!--       <li>
        <a href="kalendarz.php">
        <i class="fa fa-th"></i>
        <span>Kalendarz</span>
        </a>
      </li> -->
<!--       <li>
        <a href="historia.php">
        <i class="fa fa-th"></i>
        <span>Historia zamówień</span>
        </a>
      </li> -->
    </ul>
    <!-- sidebar menu end-->
  </div>
</aside>
<!--sidebar end-->
<?php } ?>