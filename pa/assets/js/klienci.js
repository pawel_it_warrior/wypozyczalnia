// EDYCJA KLIENTA

$(document).ready(function(){
	$('#customer-edit-form').validate({
    rules: {
	    edit_imie: {
			required: true,
	    },
	    edit_nazwisko: {
	    	required: true,
	    },
		edit_haslo: {
			required: false,
			minlength: 5,
		},
		edit_email: {
			required: true,
			email: true,
		},
	   	edit_telefon: {
			number : true,
			minlength: 6,
		},
    },
		highlight: function(element) {
			$(element).closest('.control-group').removeClass('success').addClass('error');
		},
		success: function(element) {
			element
			.text('OK!').addClass('valid')
			.closest('.control-group').removeClass('error').addClass('success');
		}
  }); // end validate()
  $(function() {
    $("#customerEditInDatabase").click(function(event) {
  		event.preventDefault();
		$('#edit_msg').html('Wpisz poprawne dane');
			var a=$('#edit_imie').val();
			var b=$('#edit_nazwisko').val();
			var d=$('#edit_haslo').val();
			var f=$('#edit_email').val();
			if (a != " " && b != " " && f != " ")
			{
				var post_data = $('#customer-edit-form').serialize();
				$.post('checker.php', post_data, function(data) {
					success:
					{
						var response = $.parseJSON(data);
						console.log(response[0].success);
						if(response[0].success == "true")
						{
							$('#customerEditInDatabase').prop('disabled', true);
							$('#edit_msg').html('<font size="100%">ZAPISANO ZMIANY</font>');
							function gothere(){
								document.location.href = 'klienci.php';
							}
							setTimeout(gothere, 1500);
						}
						else if (response[0].success == "emailexists" ){
							$('#edit_msg').html('Istnieje już użytkownik o tym adresie email');
						}
						else if (response[0].success == "dberror" ){
							$('#edit_msg').html('Nie udalo sie dodac do bazy - DBError');
						}
						else if (response[0].success == "badinfo" ){
							$('#edit_msg').html('Podano niepoprawne dane');
						}
						else if (response[0].success == "false" ){
							$('#edit_msg').html('Wpisz poprawne dane');
						}
						else $('#edit_msg').html('INNY BLAD');
					}
				});
			}
  	});
  }); // end submit click()


  // DODAWANIE KLIENTA
  $('#customer-add-form').validate({
    rules: {
	    add_imie: {
			required: true,
	    },
	    add_nazwisko: {
	    	required: true,
	    },
		add_email: {
			required: true,
			email: true,
		},
   	add_telefon: {
			number : true,
			minlength: 6,
		},
    },
	highlight: function(element) {
		$(element).closest('.control-group').removeClass('success').addClass('error');
	},
	success: function(element) {
		element
		.text('OK!').addClass('valid')
		.closest('.control-group').removeClass('error').addClass('success');
	}
  }); // end validate()
  $(function() {
    $("#customerAddToDatabase").click(function(event) {
  		event.preventDefault();
		$('#add_msg').html('Wpisz poprawne dane');
			var a=$('#add_imie').val();
			var b=$('#add_nazwisko').val();
			var c=$('#add_telefon').val();
			var d=$('#add_email').val();
			if (a != " " && b != " " && c != " " && d != " ")
			{
				var post_data = $('#customer-add-form').serialize();
				$.post('checker.php', post_data, function(data) {
					success:
					{
						var response = $.parseJSON(data);
						console.log(response[0].success);
						if(response[0].success == "true")
						{
							$('#customerAddToDatabase').prop('disabled', true);
							$('#add_msg').html('<font size="100%">DODANO KLIENTA</font>');
							function gothere(){
								document.location.href = 'klienci.php';
							}
							setTimeout(gothere, 1500);
						}
						else if (response[0].success == "emailexists" ){
							$('#add_msg').html('Istnieje już użytkownik o tym adresie email');
						}
						else if (response[0].success == "dberror" ){
							$('#add_msg').html('Nie udalo sie dodac do bazy - DBError');
						}
						else if (response[0].success == "badinfo" ){
							$('#add_msg').html('Podano niepoprawne dane');
						}
						else if (response[0].success == "false" ){
							$('#add_msg').html('Wpisz poprawne dane');
						}
						else $('#add_msg').html('INNY BLAD');
					}
				});
			}
  	});
  }); // end submit click()


    // REJESTRACJA KLIENTA
  $('#customer-register-form').validate({
    rules: {
	    register_username: {
			required: true,
	    },
	    register_password: {
	    	required: true,
	    },
    },
	highlight: function(element) {
		$(element).closest('.control-group').removeClass('success').addClass('error');
	},
	success: function(element) {
		element
		.text('OK!').addClass('valid')
		.closest('.control-group').removeClass('error').addClass('success');
	}
  }); // end validate()
  $(function() {
    $("#customerRegister").click(function(event) {
  		event.preventDefault();
		$('#register_msg').html('Wpisz poprawne dane');
			var a=$('#register_username').val();
			var b=$('#register_password').val();
			var c=$('#register_email').val();
			if (a != " " && b != " " && c != " ")
			{
				var post_data = $('#customer-register-form').serialize();
				$.post('checker.php', post_data, function(data) {
					success:
					{
						var response = $.parseJSON(data);
						console.log(response[0].success);
						if(response[0].success == "true")
						{
							$('#customerRegister').prop('disabled', true);
							$('#register_msg').html('<font size="100%">ZAREJESTROWANO KLIENTA</font>');
							function gothere(){
								document.location.href = 'klienci.php';
							}
							setTimeout(gothere, 1500);
						}
						else if (response[0].success == "userexists" ){
							$('#register_msg').html('Istnieje już użytkownik o tym adresie email');
						}
						else if (response[0].success == "dberror" ){
							$('#register_msg').html('Nie udalo sie dodac do bazy - DBError');
						}
						else if (response[0].success == "badinfo" ){
							$('#register_msg').html('Podano niepoprawne dane');
						}
						else if (response[0].success == "false" ){
							$('#register_msg').html('Wpisz poprawne dane');
						}
						else $('#register_msg').html('INNY BLAD');
					}
				});
			}
  	});
  }); // end submit click()

}); // end document.ready