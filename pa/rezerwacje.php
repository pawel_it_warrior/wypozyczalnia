<?php
  ini_set( 'display_errors', 'On' );
  error_reporting( E_ALL );
  require("config.php");
  if (empty($_SESSION['user']['username'])){
    header("Location: login.php");
  } else if ($_SESSION['user']['role'] === 'admin') {

	// function wypisz($rezerwacje){
	  // foreach($rezerwacje as $row){
	  // echo '<tr>
		// <td>'.$row['id'].'</td>
		// <td>'.$row['id_zamowienia'].'</td>
		// <td>'.$row['imie'].' '.$row['nazwisko'].'</td>
		// <td>'.$row['telefon'].'</td>
		// <td><a href="#" class="btn btn-primary btn-xs">Pokaż szczegóły</a></td>
		// </tr>';
		// }
	// }

?>
<!DOCTYPE html>
<html lang="pl">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>PANEL ADMINISTRACYJNY</title>
  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="assets/font-awesome/css/font-awesome.css"  />
  <link rel="stylesheet" type="text/css" href="assets/js/gritter/css/jquery.gritter.css" />
  <link rel="stylesheet" type="text/css" href="assets/lineicons/style.css">
  <link rel="stylesheet" type="text/css" href="assets/css/style.css">
  <link rel="stylesheet" type="text/css" href="assets/css/style-responsive.css">
  <style type="text/css">
    button.generuj:hover { background-color: #68dff0; }
  </style>
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body>
  <section id="container" >

    <?php include("header.php"); ?>

    <!-- MAIN SIDEBAR MENU -->

    <?php include("sidebar.php"); ?>

    <!-- MAIN CONTENT -->

  <!--main content start-->
  <section id="main-content">
    <section class="wrapper">

        <div class="col-lg-12 main-chart">
			<div class="col-md-12">

			  <div class="showback">
				<h4>
				</h4>
			  </div>

			<!-- TABELA -->
			  <div class="content-panel">
				<table class="table" id="tabela">
				  <thead>
					<tr style="background: #ffd777; color: #000;" id="tonie">
					  <th style="display: none;" >#</th>
					  <th>ID</th>
					  <th>ID zamowienia</th>
					  <th>Nr rezerwacji </th>
					  <th>Imie i Nazwisko</th>
					  <th>Telefon</th>
					  <th colspan="5">Operacje</th>
					</tr>
				  </thead>
				  <tbody  class="searchable" id="items" licznik='-1'>
				  </tbody>
				</table>
			  </div>
			<!-- /TABELA -->

          <!-- MODAL POPUP -->
          <div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title" id="myModalLabel">Szczegóły zamówienia</h4>
                </div>

                <!-- content -->
                <div class="modal-body" id="modal-body-content">
					<div id="skladniki_zamowienia_buty">
					</div>
					<div id="skladniki_zamowienia_narty">
					</div>
					<div id="skladniki_zamowienia_deski">
					</div>
                </div>
                <!-- /content -->

                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
                </div>

              </div>
            </div>
          </div>
          <!-- /MODAL POPUP -->
			</div>
		</div>
    </section>
  </section>
  <!--main content end-->

    <!--footer start-->
    <?php include("footer.php"); ?>
    <!--footer end-->

  </section>
  <script src="assets/js/jquery.js"></script>
  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>
  <script src="assets/js/jquery.scrollTo.min.js"></script>
  <script src="assets/js/jquery.nicescroll.js"></script>
  <script src="assets/js/jquery.sparkline.js"></script>

	<script>

	function check(){
	var s = document.getElementById('items').getAttribute('licznik');
	$.ajax({
		url: 'checker.php',
		type: 'post',
		dataType : 'json',
		data: { akcja : 'sprawdz',
				licznik : s	},
		success: function (data) {
			console.log('Ilosc: '+ data.length);
			var str = '';
			    str += '<tbody  class="searchable" id="items" licznik="-1">';
				for (var i = 0; i < data.length ; i++) {
				str += '<tr style="color: rgb(0, 0, 0); display: table-row; background: rgb(104, 223, 240);">';
				str += '<td>'+ data[i].id_r +'</td>';
				str += '<td>'+ data[i].id_z +'</td>';
				str += '<td>'+ data[i].nr_r +'</td>';
				str += '<td>'+ data[i].imie +' '+ data[i].nazwisko+ '</td>';
				str += '<td>'+ data[i].telefon +'</td>';
				str += '<td><button type="button" id="show_details" name="show_details" value="details_button_id_'+data[i].id_z+'" class="btn btn-theme">Szczegoly</button></td>';
				str += '<td><button type="button" id="accept_reservation" name="accept_reservation" value="accept_button_id_'+data[i].id_r+'" class="btn btn-theme">Zatwierdź</button></td>';
				str += '<td><button type="button" id="cancel_reservation" name="cancel_reservation" value="cancel_button_id_'+data[i].id_r+'" class="btn btn-theme">Anuluj</button></td>';
				str += '</tr>';
			  };
			  str += '</tbody>';
			  $('#items').replaceWith(str);
			document.getElementById('items').setAttribute('licznik',data.length);
		}
	});
	}
	setInterval(check,3000);

  // INNE ZDARZENIA


	// SZCZEGOLY ZAMOWIENIA
   $('body').on('click', "[value*='details_button_id_']", function() {
    var id = $(this).attr('value').replace("details_button_id_","");

	$.ajax({
		url: 'checker.php',
		type: 'post',
		dataType : 'json',
		data: { akcja1 : 'sprawdz_buty',
		        szczegoly : id},
		success: function (data) {
			var str = '';
			str += '<div id="skladniki_zamowienia_buty">';
			str += '<table class="table" id="tabela">';
			str += '<thead>';
			str += '<tr style="background: #ffd777; color: #000;" id="tonie">';
			str += '<th colspan="20">Buty</th>';
			str += '</tr>';
			str += '</thead>';
			str += '<tbody class="searchable">';
			for (var i = 0; i < data.length ; i++) {
				str += '<tr style="color: rgb(0, 0, 0); display: table-row; background: rgb(104, 223, 240);">';
				str += '<td>'+ data[i].id_zamowienia +'</td>';
				str += '<td>'+ data[i].id_buty+'</td>';
				str += '<td>'+ data[i].buty_producent_nazwa+'</td>';
				str += '<td>'+ data[i].buty_typ_nazwa+'</td>';
				str += '<td>'+ data[i].buty_rozmiar+'</td>';
				str += '<td>'+ data[i].buty_plec_nazwa+'</td>';
			str += '</tr>';
			};
			  	str += '</tbody>';
				str += '</table>';
				str += '</div>';
				$('#skladniki_zamowienia_buty').replaceWith(str);
		}
	});
	$.ajax({
		url: 'checker.php',
		type: 'post',
		dataType : 'json',
		data: { akcja1 : 'sprawdz_narty',
		        szczegoly : id},
		success: function (data) {
			var str = '';
			str += '<div id="skladniki_zamowienia_narty">';
			str += '<table class="table" id="tabela">';
			str += '<thead>';
			str += '<tr style="background: #ffd777; color: #000;" id="tonie">';
			str += '<th colspan="20">Narty</th>';
			str += '</tr>';
			str += '</thead>';
			str += '<tbody class="searchable" >';


			for (var i = 0; i < data.length ; i++) {
				str += '<tr style="color: rgb(0, 0, 0); display: table-row; background: rgb(104, 223, 240);">';
				str += '<td>'+ data[i].id_zamowienia +'</td>';
				str += '<td>'+ data[i].id_buty+'</td>';
				str += '<td>'+ data[i].narty_producent_nazwa+'</td>';
				str += '<td>'+ data[i].narty_typ_nazwa+'</td>';
				str += '<td>'+ data[i].narty_rozmiar+'</td>';
				str += '<td>'+ data[i].narty_plec_nazwa+'</td>';
			str += '</tr>';
			};
			  	str += '</tbody>';
				str += '</table>';
				str += '</div>';
				$('#skladniki_zamowienia_narty').replaceWith(str);
		}
	});
	$.ajax({
		url: 'checker.php',
		type: 'post',
		dataType : 'json',
		data: { akcja1 : 'sprawdz_deski',
		        szczegoly : id},
		success: function (data) {
			var str = '';
			str += '<div id="skladniki_zamowienia_deski">';
			str += '<table class="table" id="tabela">';
			str += '<thead>';
			str += '<tr style="background: #ffd777; color: #000;" id="tonie">';
			str += '<th colspan="20">Deski</th>';
			str += '</tr>';
			str += '</thead>';
			str += '<tbody class="searchable" >';

			for (var i = 0; i < data.length ; i++) {
				str += '<tr style="color: rgb(0, 0, 0); display: table-row; background: rgb(104, 223, 240);">';
				str += '<td>'+ data[i].id_zamowienia +'</td>';
				str += '<td>'+ data[i].id_buty+'</td>';
				str += '<td>'+ data[i].deski_producent_nazwa+'</td>';
				str += '<td>'+ data[i].deski_typ_nazwa+'</td>';
				str += '<td>'+ data[i].deski_rozmiar+'</td>';
				str += '<td>'+ data[i].deski_plec_nazwa+'</td>';
			str += '</tr>';
			};
			  	str += '</tbody>';
				str += '</table>';
				str += '</div>';
				$('#skladniki_zamowienia_deski').replaceWith(str);
		}
	});


	var options = {
      "backdrop" : "static",
      "keyboard" : "true"
    }
    $('#basicModal').modal(options);


   });
// KONIEC SZCZEGOLOW ZAMOWIENIA
  $('body').on('click', ".table tr", function() {
    if ($(this).attr("id") != "tonie"){
      $(".table tr").each(function () {
        $(this).attr("class", "");
      });
        $(this).attr("class", "active");
    }

  });


  $('body').on('click', "[value*='accept_button_id_']", function() {
    var id = $(this).attr('value').replace("accept_button_id_","");
	alert('akceptuj rezerwacje o id - ' + id);

  })

    $('body').on('click', "[value*='cancel_button_id_']", function() {
    var id = $(this).attr('value').replace("cancel_button_id_","");
	alert('anuluj rezerwacje o id - ' + id);
  })


	</script>
</body>
</html>
<?php } ?>