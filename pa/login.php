<?php
require ("config.php");

$submitted_username = '';
if (!empty($_POST)) {
  $query_params = array(':username' => $_POST['username']);
  $query = "SELECT id, imie, nazwisko, username, password, salt, email, telefon, role FROM users WHERE username = :username LIMIT 1";
  try {
    $stmt = $db->prepare($query);
    $result = $stmt->execute($query_params);
  }
  catch(PDOException $ex) {
    die("Failed to run query: " . $ex->getMessage());
  }
  $logged = false;
  $row = $stmt->fetch();
  if ($row) {
    $check_password = hash('sha256', $_POST['password'] . $row['salt']);
    for ($round = 0; $round < 65536; $round++) {
      $check_password = hash('sha256', $check_password . $row['salt']);
    }
    if ($check_password === $row['password']) {
      $logged = true;
    }
  }
  if ($logged && $row['role'] === 'admin') {
    unset($row['salt']);
    unset($row['password']);
    $_SESSION['user'] = $row;
    header("Location: index.php");
  }
  else {
    $submitted_username = htmlentities($_POST['username'], ENT_QUOTES, 'UTF-8');
    header("Location: login.php");
  }
}
?>

<!DOCTYPE html>
<html lang="pl">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Zaloguj</title>
  <link href="assets/css/bootstrap.css" rel="stylesheet">
  <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link href="assets/css/style.css" rel="stylesheet">
  <link href="assets/css/style-responsive.css" rel="stylesheet">
  <style type="text/css">
  .bgimg {
    color: rgba(255,255,255,0.8);
    background-color: #ffd777;
  }
  </style>
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body class="bgimg">

  <div id="login-page">
    <div class="container">
      <form role="form" action="login.php" method="POST" class="form-login" name="logowanie">
        <h2 class="form-login-heading">Logowanie do systemu</h2>
        <div class="login-wrap">
          <input type="text" name="username" class="form-control" placeholder="Nazwa użytkownika">
          <br>
          <input type="password" name="password" class="form-control" placeholder="Hasło">
          <br>
          <button type="submit" name="zaloguj" class="btn btn-theme btn-block"><i class="fa fa-lock"></i> Zaloguj</button>
        </div>
      </form>
    </div>
  </div>

</body>
</html>
