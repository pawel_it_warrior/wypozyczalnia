<?php
  ini_set( 'display_errors', 'On' );
  error_reporting( E_ALL );
  require("config.php");
  if (empty($_SESSION['user']['username'])){
    header("Location: login.php");
  } else if ($_SESSION['user']['role'] === 'admin') {

    // pobieranie danych wydzialu
    $query = "SELECT COUNT(id) AS totalcustomers FROM users ";
    try {
      $stmt = $db->prepare($query);
      $result = $stmt->execute();
    } catch (PDOException $ex){ die("Failed to run query: " . $ex->getMessage()); }
    $r = $stmt->fetch();
    $totalcustomers = $r['totalcustomers'];

?>
<!DOCTYPE html>
<html lang="pl">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>PANEL ADMINISTRACYJNY</title>
  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="assets/font-awesome/css/font-awesome.css"  />
  <link rel="stylesheet" type="text/css" href="assets/js/gritter/css/jquery.gritter.css" />
  <link rel="stylesheet" type="text/css" href="assets/lineicons/style.css">
  <link rel="stylesheet" type="text/css" href="assets/css/style.css">
  <link rel="stylesheet" type="text/css" href="assets/css/style-responsive.css">
  <link rel="stylesheet" href="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
  <style type="text/css">
    button.generuj:hover { background-color: #68dff0; }
  </style>
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body>
  <section id="container" >

    <?php include("header.php"); ?>

    <!-- MAIN SIDEBAR MENU -->

    <?php include("sidebar.php"); ?>

    <!-- MAIN CONTENT -->

    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <div class="row">


          <!-- TUTAJ JEST MIEJSCE GDZIE WRZUCAM ALL-->
          <div class="col-md-6 main-chart">
          <div class="showback">
            <h4 style="text-align: right;"><i class="fa fa-angle-right"></i>
              <span style="text-align: left;">Ilość klientów w bazie</span>
              <span style="text-align: right; margin-left: 100px;"><b><?php echo $totalcustomers; ?></b></span>
            </h4>
          </div>
          <div class="showback">
            <h4 style="text-align: right;"><i class="fa fa-angle-right"></i>
              <span style="text-align: left;">Do dopisania</span>
              <span style="text-align: right; margin-left: 100px;"><b>12</b></span>
            </h4>
          </div>
          </div>
          <div class="col-md-6 main-chart">
          <div class="showback">
            <h4 style="text-align: right;"><i class="fa fa-angle-right"></i>
              <span style="text-align: left;">Ilość klientów w bazie</span>
              <span style="text-align: right; margin-left: 100px;"><b><?php echo $totalcustomers; ?></b></span>
            </h4>
          </div>
          <div class="showback">
            <h4 style="text-align: right;"><i class="fa fa-angle-right"></i>
              <span style="text-align: left;">Cokolwiek</span>
              <span style="text-align: right; margin-left: 100px;"><b>12</b></span>
            </h4>
          </div>
          </div>

          <!-- wykres -->
          <div class="col-md-12" align="center"><legend>Wykres miesięcznych wypożyczeń za rok <?php echo date("Y")-1;?></legend><div class="ct-chart ct-perfect-fourth"></div></div>

          </div>
        </div>
      </section>
    </section>
    <!--main content end-->

    <!--footer start-->
    <?php include("footer.php"); ?>
    <!--footer end-->

  </section>
  <script src="assets/js/jquery.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>
  <script src="assets/js/jquery.scrollTo.min.js"></script>
  <script src="assets/js/jquery.nicescroll.js"></script>
  <script src="assets/js/jquery.sparkline.js"></script>
  <script src="assets/js/jquery.simplePagination.js"></script>
  <script src="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>
  <script>
    var chart = new Chartist.Line('.ct-chart', {
    labels: ['Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec', 'Lipiec', 'Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień'],
    series: [
      // [12, 11, 10, 2, 1, 0, 0, 1, 0, 17, 24],
      [10, 15, 11, 1, 0, 0, 0, 0, 2, 3, 18, 26],
    ]
    }, {
    // Remove this configuration to see that chart rendered with cardinal spline interpolation
    // Sometimes, on large jumps in data values, it's better to use simple smoothing.
    lineSmooth: Chartist.Interpolation.simple({
      divisor: 2
    }),
    fullWidth: true,
    height: 300,
    chartPadding: {
      right: 20
    },
    low: 0
    });
  </script>
</body>
</html>
<?php } ?>