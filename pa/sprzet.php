<?php
require ("config.php");

if (empty($_SESSION['user']['username'])) {
  header("Location: login.php");
}
else if ($_SESSION['user']['role'] === 'admin') {

  $query = "SELECT * FROM Buty_Producent";
  try {
    $stmt = $db->prepare($query);
    $result = $stmt->execute();
  }
  catch(PDOException $ex) {
    die("Failed to run query: " . $ex->getMessage());
  }
  $buty_producent = Array();
  while ($r = $stmt->fetch()) {
    array_push($buty_producent, $r['buty_producent_nazwa']);
  }

  $query = "SELECT * FROM Buty_Rozmiar";
  try {
    $stmt = $db->prepare($query);
    $result = $stmt->execute();
  }
  catch(PDOException $ex) {
    die("Failed to run query: " . $ex->getMessage());
  }
  $buty_rozmiar = Array();
  while ($r = $stmt->fetch()) {
    array_push($buty_rozmiar, $r['buty_rozmiar']);
  }

  $query = "SELECT * FROM Narty_Producent";
  try {
    $stmt = $db->prepare($query);
    $result = $stmt->execute();
  }
  catch(PDOException $ex) {
    die("Failed to run query: " . $ex->getMessage());
  }
  $narty_producent = Array();
  while ($r = $stmt->fetch()) {
    array_push($narty_producent, $r['narty_producent_nazwa']);
  }

  $query = "SELECT * FROM Narty_Rozmiar";
  try {
    $stmt = $db->prepare($query);
    $result = $stmt->execute();
  }
  catch(PDOException $ex) {
    die("Failed to run query: " . $ex->getMessage());
  }
  $narty_rozmiar = Array();
  while ($r = $stmt->fetch()) {
    array_push($narty_rozmiar, $r['narty_rozmiar']);
  }

  $query = "SELECT * FROM Deski_Producent";
  try {
    $stmt = $db->prepare($query);
    $result = $stmt->execute();
  }
  catch(PDOException $ex) {
    die("Failed to run query: " . $ex->getMessage());
  }
  $deski_producent = Array();
  while ($r = $stmt->fetch()) {
    array_push($deski_producent, $r['deski_producent_nazwa']);
  }

  $query = "SELECT * FROM Deski_Rozmiar";
  try {
    $stmt = $db->prepare($query);
    $result = $stmt->execute();
  }
  catch(PDOException $ex) {
    die("Failed to run query: " . $ex->getMessage());
  }
  $deski_rozmiar = Array();
  while ($r = $stmt->fetch()) {
    array_push($deski_rozmiar, $r['deski_rozmiar']);
  }


?>

<!DOCTYPE html>
<html lang="pl">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>PANEL ADMINISTRACYJNY</title>
  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="assets/font-awesome/css/font-awesome.css"  />
  <link rel="stylesheet" type="text/css" href="assets/lineicons/style.css">
  <link rel="stylesheet" type="text/css" href="assets/css/style.css">
  <link rel="stylesheet" type="text/css" href="assets/css/style-responsive.css">
  <style type="text/css">
    .highlighted, tr.highlighted {
      background-color: #5bc0de;
      color: black;
    }
    .table-striped>tbody>tr:nth-child(odd)>th {
    }
    .table-striped>tbody>tr:nth-child(odd)>td {
    }
    td:first-letter {
        text-transform: uppercase;
    }
    button.generuj:hover { background-color: #68dff0; }
  </style>
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body>
<section id="container" >

  <?php
  include ("header.php"); ?>

  <?php
  include ("sidebar.php"); ?>

  <!--main content start-->
  <section id="main-content">
    <section class="wrapper">

        <div class="col-lg-12 main-chart">
          <div class="col-md-12">
            <h4>
            <button type="button" id="add" class="btn btn-theme">Dodaj nowy sprzęt</button>
            </h4>

          <!-- TABELA -->
          <div class="content-panel"  style="padding-bottom: 5px; padding-top: 0px;">

            <div class="input-group"> <span class="input-group-addon">Filtruj</span>
              <select id="filter" class="form-control">
                <option value=""></option>
                <option value="Buty">Buty</option>
                <option value="Narty">Narty</option>
                <option value="Deski">Deski</option>
              </select>
            </div>

            <table class="table" id="tabela" style="margin-bottom: 15px;">
              <thead>
                <tr style="background: #ffd777; color: #000;" id="tonie">
                  <th>Rodzaj sprzętu</th>
                  <th style="display: none;" >UID</th>
                  <th>Model</th>
                  <th>Producent</th>
                  <th>Typ</th>
                  <th>Zaawansowanie</th>
                  <th>Płeć</th>
                  <th>Rozmiar</th>
                  <th>Wypozyczone</th>
                  <th>Usuń</th>
                </tr>
              </thead>
              <tbody  class="searchable" id="items">

              </tbody>
            </table>
          </div>
          <!-- /TABELA -->

          <!-- MODAL POPUP -->
          <div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title" id="myModalLabel">Dodawanie nowego sprzętu</h4>
                </div>

                <!-- content -->
                <div class="modal-body">

                  <!-- main select -->
                  <select class="form-control" style="background-color: #ccc; margin-bottom: 10px;" id="rodzaj_sprzetu" name="rodzaj_sprzetu">
                    <option value="">Wybierz sprzęt</option>
                    <option value="buty">Buty</option>
                    <option value="narty">Narty</option>
                    <option value="deski">Deski</option>
                  </select>

                  <!-- FORMA DLA BUTÓW -->
                  <div id="container_buty">
                    <select class="form-control" style="background-color: #ccc; margin-bottom: 10px;" id="select_buty_producent" name="select_buty_producent">
                      <option selected="true" value="">Wybierz producenta</option>
                        <?php
                        foreach ($buty_producent as $bp) {
                          echo '<option value="' . $bp . '">' . $bp . '</option>';
                        } ?>
                      <option>---</option>
                      <option value="buty_producent_nowy"><b>Dodaj nowego producenta</b></option>
                    </select>

                    <div id="buty_producent_nowy" >
                      <div class="col-md-2" style="padding-left:0px; padding-right: 0px;">
                        <button type="button" class="btn btn-primary" style="width:100%;" id="buty_producent_nowy_dodaj">Dodaj</button>
                      </div>
                      <div class="col-md-10" style="padding-right: 0px;">
                        <input type="text" class="form-control" name="buty_producent_nowy_input" id="buty_producent_nowy_input" />
                      </div>
                    </div>
                    <div class="col-md-2" style="padding-left:0px; padding-right: 0px;"><h4>Model</h4></div>
                    <div class="col-md-10" style="padding-right: 0px;">
                      <input type="text" class="form-control" name="buty_model" id="buty_model" />
                    </div>
                    <select class="form-control" style="background-color: #ccc; margin-bottom: 10px;" id="select_buty_rozmiar" name="select_buty_rozmiar">
                      <option selected="true" value="">Wybierz rozmiar</option>
                      <?php
                      foreach ($buty_rozmiar as $br) {
                        echo '<option value="' . $br . '">' . $br . '</option>';
                      } ?>
                    </select>
                    <select class="form-control" style="background-color: #ccc; margin-bottom: 10px;" id="select_buty_typ" name="select_buty_typ">
                      <option selected="true" value="">Wybierz typ</option>
                      <option value="All mountain">All mountain</option>
                      <option value="Carving">Carving</option>
                      <option value="Free style">Free style</option>
                      <option value="Ride">Ride</option>
                      <option value="Junior">Junior</option>
                      <option value="Soft">Soft</option>
                      <option value="Telemark">Telemark</option>
                      <option value="Uniwersalne">Uniwersalne</option>
                    </select>
                    <select class="form-control" style="background-color: #ccc; margin-bottom: 10px;" id="select_buty_plec" name="select_buty_plec">
                      <option selected="true" value="">Wybierz płeć</option>
                      <option value="Unisex">Unisex</option>
                      <option value="Kobiety">Kobiety</option>
                      <option value="Dzieci i młodzież">Dzieci i młodzież</option>
                    </select>
                    <select class="form-control" style="background-color: #ccc; margin-bottom: 10px;" id="select_buty_zaawansowanie" name="select_buty_zaawansowanie">
                      <option selected="true" value="">Wybierz zaawansowanie</option>
                      <option value="Początkujący">Początkujący</option>
                      <option value="Ekspert">Ekspert</option>
                      <option value="Średnio zaawansowany">Średnio zaawansowany</option>
                      <option value="Zaawansowany">Zaawansowany</option>
                      <option value="Zawodnik">Zawodnik</option>
                    </select>
                  </div>
                  <!-- /FORMA DLA BUTÓW -->


                  <!-- FORMA DLA NART -->
                  <div id="container_narty">
                    <select class="form-control" style="background-color: #ccc; margin-bottom: 10px;" id="select_narty_producent" name="select_narty_producent">
                      <option selected="true" value="">Wybierz producenta</option>
                      <?php
                        foreach ($narty_producent as $bp) {
                          echo '<option value="' . $bp . '">' . $bp . '</option>';
                        } ?>
                      <option>---</option>
                      <option value="narty_producent_nowy"><b>Dodaj nowego producenta</b></option>
                    </select>
                    <div id="narty_producent_nowy" >
                      <div class="col-md-2" style="padding-left:0px; padding-right: 0px;">
                        <button type="button" class="btn btn-primary" style="width:100%;" id="narty_producent_nowy_dodaj">Dodaj</button>
                      </div>
                      <div class="col-md-10" style="padding-right: 0px;">
                        <input type="text" class="form-control" name="narty_producent_nowy_input" id="narty_producent_nowy_input" />
                      </div>
                    </div>
                    <div class="col-md-2" style="padding-left:0px; padding-right: 0px;"><h4>Model</h4></div>
                    <div class="col-md-10" style="padding-right: 0px;">
                      <input type="text" class="form-control" name="narty_model" id="narty_model" />
                    </div>
                    <select class="form-control" style="background-color: #ccc; margin-bottom: 10px;" id="select_narty_rozmiar" name="select_narty_rozmiar">
                      <option selected="true" value="">Wybierz rozmiar</option>
                      <?php
                      foreach ($narty_rozmiar as $br) {
                        echo '<option value="' . $br . '">' . $br . '</option>';
                      } ?>
                    </select>
                    <select class="form-control" style="background-color: #ccc; margin-bottom: 10px;" id="select_narty_typ" name="select_narty_typ">
                      <option selected="true" value="">Wybierz typ</option>
                      <option value="All mountain">All mountain</option>
                      <option value="Carving">Carving</option>
                      <option value="Free style">Free style</option>
                      <option value="Ride">Ride</option>
                      <option value="Junior">Junior</option>
                      <option value="On pist">On pist</option>
                      <option value="Telemark">Telemark</option>
                      <option value="Uniwersalne">Uniwersalne</option>
                      <option value="Cross">Cross</option>
                    </select>
                    <select class="form-control" style="background-color: #ccc; margin-bottom: 10px;" id="select_narty_plec" name="select_narty_plec">
                      <option selected="true" value="">Wybierz płeć</option>
                      <option value="Unisex">Unisex</option>
                      <option value="Kobiety">Kobiety</option>
                      <option value="Dzieci i młodzież">Dzieci i młodzież</option>
                    </select>
                    <select class="form-control" style="background-color: #ccc; margin-bottom: 10px;" id="select_narty_zaawansowanie" name="select_narty_zaawansowanie">
                      <option selected="true" value="">Wybierz zaawansowanie</option>
                      <option value="Początkujący">Początkujący</option>
                      <option value="Ekspert">Ekspert</option>
                      <option value="Średnio zaawansowany">Średnio zaawansowany</option>
                      <option value="Zaawansowany">Zaawansowany</option>
                      <option value="Zawodnik">Zawodnik</option>
                    </select>
                  </div>
                  <!-- /FORMA DLA NART -->


                   <!-- FORMA DLA DESEK -->
                  <div id="container_deski">
                    <select class="form-control" style="background-color: #ccc; margin-bottom: 10px;" id="select_deski_producent" name="select_deski_producent">
                      <option selected="true" value="">Wybierz producenta</option>
                      <?php
                      foreach ($deski_producent as $bp) {
                        echo '<option value="' . $bp . '">' . $bp . '</option>';
                      } ?>
                      <option>---</option>
                      <option value="deski_producent_nowy"><b>Dodaj nowego producenta</b></option>
                    </select>
                    <div id="deski_producent_nowy" >
                      <div class="col-md-2" style="padding-left:0px; padding-right: 0px;">
                        <button type="button" class="btn btn-primary" style="width:100%;" id="deski_producent_nowy_dodaj">Dodaj</button>
                      </div>
                      <div class="col-md-10" style="padding-right: 0px;">
                        <input type="text" class="form-control" name="deski_producent_nowy_input" id="deski_producent_nowy_input" />
                      </div>
                    </div>
                    <div class="col-md-2" style="padding-left:0px; padding-right: 0px;"><h4>Model</h4></div>
                    <div class="col-md-10" style="padding-right: 0px;">
                      <input type="text" class="form-control" name="deski_model" id="deski_model" />
                    </div>
                    <select class="form-control" style="background-color: #ccc; margin-bottom: 10px;" id="select_deski_rozmiar" name="select_deski_rozmiar">
                      <option selected="true" value="">Wybierz rozmiar</option>
                      <?php
                      foreach ($deski_rozmiar as $br) {
                        echo '<option value="' . $br . '">' . $br . '</option>';
                      } ?>
                    </select>
                    <select class="form-control" style="background-color: #ccc; margin-bottom: 10px;" id="select_deski_typ" name="select_deski_typ">
                      <option selected="true" value="">Wybierz typ</option>
                      <option value="All mountain">All mountain</option>
                      <option value="Carving">Carving</option>
                      <option value="Free style">Free style</option>
                      <option value="Ride">Ride</option>
                      <option value="Junior">Junior</option>
                      <option value="On pist">On pist</option>
                      <option value="Telemark">Telemark</option>
                      <option value="Uniwersalne">Uniwersalne</option>
                      <option value="Cross">Cross</option>
                    </select>
                    <select class="form-control" style="background-color: #ccc; margin-bottom: 10px;" id="select_deski_plec" name="select_deski_plec">
                      <option selected="true" value="">Wybierz płeć</option>
                      <option value="Unisex">Unisex</option>
                      <option value="Kobiety">Kobiety</option>
                      <option value="Dzieci i młodzież">Dzieci i młodzież</option>
                    </select>
                    <select class="form-control" style="background-color: #ccc; margin-bottom: 10px;" id="select_deski_zaawansowanie" name="select_deski_zaawansowanie">
                      <option selected="true" value="">Wybierz zaawansowanie</option>
                      <option value="Początkujący">Początkujący</option>
                      <option value="Ekspert">Ekspert</option>
                      <option value="Średnio zaawansowany">Średnio zaawansowany</option>
                      <option value="Zaawansowany">Zaawansowany</option>
                      <option value="Zawodnik">Zawodnik</option>
                    </select>
                  </div>
                  <!-- /FORMA DLA DESEK -->

                </div>
                <!-- /content -->

                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
                  <button type="button" class="btn btn-primary" id="dodaj" >Dodaj</button>
                </div>

              </div>
            </div>
          </div>
          <!-- /MODAL POPUP -->

          </div>
      </div>
    </section>
  </section>
  <!--main content end-->

  <!--footer start-->
  <?php
  include ("footer.php"); ?>
  <!--footer end-->

  </section>
  <script src="assets/js/jquery.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>
  <script src="assets/js/jquery.scrollTo.min.js"></script>
  <script src="assets/js/jquery.nicescroll.js"></script>
  <script src="assets/js/jquery.sparkline.js"></script>
  <script src="assets/js/jquery.simplePagination.js"></script>

  <script>
  function pobierz_dane(){


  $.ajax({
    type:"POST",
    url:"ajax.php",
    data:{ action : "pull" },
    dataType: 'text',
    success:function(data){
      var response = $.parseJSON(data);
      $('#items').empty();
      for (var i = 0; i < response.length - 1; i++) {
        var str = '';
        if (response[i].wypozyczenie == 0)
          str += '<tr style="background: #68dff0; color: #000;">';
        else
          str += '<tr style="background: #ed5565; color: #000;">';

        str += '<td>'+ response[i].rodzaj +'</td>';

        if (response[i].rodzaj == "buty" ){
          str += '<td style="display: none;" id="'+ response[i].uid +'" >'+ response[i].uid +'</td>';
          str += '<td>'+response[i].buty_model+'</td>';
          str += '<td>'+response[i].buty_producent_nazwa+'</td>';
          str += '<td>'+response[i].buty_typ_nazwa+'</td>';
          str += '<td>'+response[i].buty_zaawansowanie_nazwa+'</td>';
          str += '<td>'+response[i].buty_plec_nazwa+'</td>';
          str += '<td>'+response[i].buty_rozmiar+'</td>';
          if (response[i].wypozyczenie==0)
            str += '<td>Na magazynie</td>';
          else
            str += '<td>Wypozyczony</td>';
          str += '<td><button style="padding: 1px;font-size: 80%;" type="button" class="btn btn-theme04" value="usun_'+response[i].uid+'" >Usuń</button></td>';
          str += '</tr>';
        }

        if (response[i].rodzaj == "narty" ){
          str += '<td style="display: none;" id="'+ response[i].uid +'" >'+ response[i].uid +'</td>';
          str += '<td>'+response[i].narty_model+'</td>';
          str += '<td>'+response[i].narty_producent_nazwa+'</td>';
          str += '<td>'+response[i].narty_typ_nazwa+'</td>';
          str += '<td>'+response[i].narty_zaawansowanie_nazwa+'</td>';
          str += '<td>'+response[i].narty_plec_nazwa+'</td>';
          str += '<td>'+response[i].narty_rozmiar+'</td>';
          if (response[i].wypozyczenie==0)
            str += '<td>Na magazynie</td>';
          else
            str += '<td>Wypozyczony</td>';
          str += '<td><button style="padding: 1px;font-size: 80%;" type="button" class="btn btn-theme04" value="usun_'+response[i].uid+'" >Usuń</button></td>';
          str += '</tr>';
        }

        if (response[i].rodzaj == "deski" ){
          str += '<td style="display: none;" id="'+ response[i].uid +'" >'+ response[i].uid +'</td>';
          str += '<td>'+response[i].deski_model+'</td>';
          str += '<td>'+response[i].deski_producent_nazwa+'</td>';
          str += '<td>'+response[i].deski_typ_nazwa+'</td>';
          str += '<td>'+response[i].deski_zaawansowanie_nazwa+'</td>';
          str += '<td>'+response[i].deski_plec_nazwa+'</td>';
          str += '<td>'+response[i].deski_rozmiar+'</td>';
          if (response[i].wypozyczenie==0)
            str += '<td>Na magazynie</td>';
          else
            str += '<td>Wypozyczony</td>';
          str += '<td><button style="padding: 1px;font-size: 80%;" type="button" class="btn btn-theme04" value="usun_'+response[i].uid+'" >Usuń</button></td>';
          str += '</tr>';
        }
        $('#items').append(str);
      };
    $('#paginacja').remove();
    $("#tabela").simplePagination();
    }
  });
 }
 pobierz_dane();

  $('body').on('click', "[value*='ok_']", function() {
    var id = $(this).attr('value').replace("ok_","");
    $('#'+id).addClass('highlighted');
    if ( $.inArray(id, orders)  == -1 ) {
      orders.push(id);
    }
    $('#ilosctowarow').text( orders.length );
  });


  $('body').on('click', "[value*='usun_']", function() {
    var row = $(this).closest("tr");
    var tableData = row.children("td").map(function() {
      return $(this).text();
    }).get();
    $.ajax({
      type: "POST",
      data: { action : "usun_sprzet", rodzaj : tableData[0], uid : tableData[1] },
      url: "ajax.php"
    });
    pobierz_dane();
  });


  $('body').on('click', ".table tr", function() {
    if ($(this).attr("id") != "tonie"){
      $(".table tr").each(function () {
        $(this).attr("class", "");
      });
        $(this).attr("class", "active");
    }
  });

  $('#filter').change(function () {
    var rex = new RegExp($(this).val(), 'i');
    $('.searchable tr').hide();
    $('.searchable tr').filter(function () {
      return rex.test($(this).text());
    }).show();
  });

  $('#add').click(function(){
    var options = {
      "backdrop" : "static",
      "keyboard" : "true"
    }
    $('#basicModal').modal(options);
    $('#container_buty').hide();
    $('#container_narty').hide();
    $('#container_deski').hide();
    $('#buty_producent_nowy').hide();
    $('#narty_producent_nowy').hide();
    $('#deski_producent_nowy').hide();

    $('#rodzaj_sprzetu').on('change', function(){
      if ($('rodzaj_sprzetu').val() != 0) {
        if( $('#rodzaj_sprzetu option:selected').val() == 'buty' ){
          $('#container_narty').hide();
          $('#container_deski').hide();
          $('#container_buty').show();
        }
        if( $('#rodzaj_sprzetu option:selected').val() == 'narty' ){
          $('#container_buty').hide();
          $('#container_deski').hide();

        }
        if( $('#rodzaj_sprzetu option:selected').val() == 'deski' ){
          $('#container_narty').hide();
          $('#container_buty').hide();
          $('#container_deski').show();
        }
      }
    });

    $('#select_buty_producent').on('change', function(){
      if ( $('#select_buty_producent option:selected').val() == 'buty_producent_nowy' ){
        $('#buty_producent_nowy').show();
      } else{
        $('#buty_producent_nowy').hide();
        $('#buty_producent_nowy_input').val('');
      }
    });

    $('#select_narty_producent').on('change', function(){
      if ( $('#select_narty_producent option:selected').val() == 'narty_producent_nowy' ){
        $('#narty_producent_nowy').show();
      } else{
        $('#narty_producent_nowy').hide();
        $('#narty_producent_nowy_input').val('');
      }
    });

    $('#select_deski_producent').on('change', function(){
      if ( $('#select_deski_producent option:selected').val() == 'deski_producent_nowy' ){
        $('#deski_producent_nowy').show();
      } else{
        $('#deski_producent_nowy').hide();
        $('#deski_producent_nowy_input').val('');
      }
    });

    $('#buty_producent_nowy_dodaj').click(function(event){
      event.preventDefault();
      if ( $('#buty_producent_nowy_input').val() != "" ){
        var nowy = $('#buty_producent_nowy_input').val();
        $.ajax({
          type: "POST",
          data: { buty_producent_nowy_value : nowy },
          url: "ajax.php",
          success: function(data) {
            var response = $.parseJSON(data);
            if (response[0].success == "true"){
              var select = $('#select_buty_producent');
              $('#select_buty_producent')
                .find('option')
                .remove()
                .end();
              var i = 1;
              $('#select_buty_producent').append('<option selected="true" value="">Wybierz producenta</option>');
              while ( i < response.length ){
                var row='<option value=' + response[i].producent + '>' + response[i].producent + '</option>';
                $("#select_buty_producent").append(row);
                i++;
              }
              $('#select_buty_producent')
                .append('<option>---</option>')
                .append('<option value="buty_producent_nowy"><b>Dodaj nowego producenta</b></option>');
              $('#buty_producent_nowy_input').val('');
              $('#buty_producent_nowy').hide();
            }
          }
        });
      } else {
        alert('Proszę uzupełnić pole z nazwą producenta.');
      }
    });

    $('#narty_producent_nowy_dodaj').click(function(event){
      event.preventDefault();
      if ( $('#narty_producent_nowy_input').val() != "" ){
        var nowy = $('#narty_producent_nowy_input').val();
        $.ajax({
          type: "POST",
          data: { narty_producent_nowy_value : nowy },
          url: "ajax.php",
          success: function(data) {
            var response = $.parseJSON(data);
            if (response[0].success == "true"){
              var select = $('#select_narty_producent');
              $('#select_narty_producent')
                .find('option')
                .remove()
                .end();
              var i = 1;
              $('#select_narty_producent').append('<option selected="true" value="">Wybierz producenta</option>');
              while ( i < response.length ){
                var row='<option value=' + response[i].producent + '>' + response[i].producent + '</option>';
                $("#select_narty_producent").append(row);
                i++;
              }
              $('#select_buty_producent')
                .append('<option>---</option>')
                .append('<option value="narty_producent_nowy"><b>Dodaj nowego producenta</b></option>');
              $('#narty_producent_nowy_input').val('');
              $('#narty_producent_nowy').hide();
            }
          }
        });
      } else {
        alert('Proszę uzupełnić pole z nazwą producenta.');
      }
    });

    $('#deski_producent_nowy_dodaj').click(function(event){
      event.preventDefault();
      if ( $('#deski_producent_nowy_input').val() != "" ){
        var nowy = $('#deski_producent_nowy_input').val();
        $.ajax({
          type: "POST",
          data: { deski_producent_nowy_value : nowy },
          url: "ajax.php",
          success: function(data) {
            var response = $.parseJSON(data);
            if (response[0].success == "true"){
              var select = $('#select_deski_producent');
              $('#select_deski_producent')
                .find('option')
                .remove()
                .end();
              var i = 1;
              $('#select_deski_producent').append('<option selected="true" value="">Wybierz producenta</option>');
              while ( i < response.length ){
                var row='<option value=' + response[i].producent + '>' + response[i].producent + '</option>';
                $("#select_deski_producent").append(row);
                i++;
              }
              $('#select_deski_producent')
                .append('<option>---</option>')
                .append('<option value="deski_producent_nowy"><b>Dodaj nowego producenta</b></option>');
              $('#deski_producent_nowy_input').val('');
              $('#deski_producent_nowy').hide();
            }
          }
        });
      } else {
        alert('Proszę uzupełnić pole z nazwą producenta.');
      }
    });

  });

  $('#dodaj').click(function(){
    var rodzaj = $('#rodzaj_sprzetu').val();
    if (rodzaj !== ""){
      var producent = $('#select_'+rodzaj+'_producent').val();
      var model = $('#'+rodzaj+'_model').val();
      var rozmiar = $('#select_'+rodzaj+'_rozmiar').val();
      var typ = $('#select_'+rodzaj+'_typ').val();
      var plec = $('#select_'+rodzaj+'_plec').val();
      var zaawansowanie = $('#select_'+rodzaj+'_zaawansowanie').val();
      if ( producent != "" && model != "" && rozmiar != "" && typ != "" && plec != "" && zaawansowanie != "" ){
        $.ajax({
          type:"POST",
          url:"ajax.php",
          data:{ action : "dodaj_sprzet", rodzaj:rodzaj, producent:producent, model:model, rozmiar:rozmiar, typ:typ, plec:plec, zaawansowanie:zaawansowanie },
          dataType: 'text',
          success:function(data){
            // var response = $.parseJSON(data);
            console.log(data);
          }
        });
        $('#basicModal').modal('hide');
        $('#tabela tr:last').after('<tr style="background: #68dff0; color: #000;"><td style="display: none;">UID</td><td>'+rodzaj+'</td><td>'+model+'</td><td>'+producent+'</td><td>'+typ+'</td><td>'+zaawansowanie+'</td><td>'+plec+'</td><td>'+rozmiar+'</td><td>Wolny</td></tr>');
      }
    }
  });

  </script>
</body>
</html>
<?php } ?>