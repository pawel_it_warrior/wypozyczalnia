<?php
require ("config.php");
if (empty($_SESSION['user']['username'])) {
  header("Location: login.php");
}
else if ($_SESSION['user']['role'] === 'admin') {
?>
<!DOCTYPE html>
<html lang="pl">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>PANEL ADMINISTRACYJNY</title>
  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="assets/font-awesome/css/font-awesome.css"  />
  <link rel="stylesheet" type="text/css" href="assets/js/gritter/css/jquery.gritter.css" />
  <link rel="stylesheet" type="text/css" href="assets/lineicons/style.css">
  <link rel="stylesheet" type="text/css" href="assets/css/style.css">
  <link rel="stylesheet" type="text/css" href="assets/css/style-responsive.css">
  <style type="text/css">
    button.generuj:hover { background-color: #68dff0; }
  </style>
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body>
  <section id="container" >

    <?php include ("header.php"); ?>

    <!-- MAIN SIDEBAR MENU -->

    <?php include ("sidebar.php"); ?>

    <!-- MAIN CONTENT -->

    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <div class="row">
          <div class="col-lg-12 main-chart">



          </div>
        </div>
      </section>
    </section>
    <!--main content end-->

    <!--footer start-->
    <?php include ("footer.php"); ?>
    <!--footer end-->

  </section>

  <script src="assets/js/jquery.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>
  <script src="assets/js/jquery.scrollTo.min.js"></script>
  <script src="assets/js/jquery.nicescroll.js"></script>
  <script src="assets/js/jquery.sparkline.js"></script>
  <script src="assets/js/jquery.simplePagination.js"></script>

</body>
</html>
<?php } ?>