<?php
    $query = "SELECT uid FROM Buty WHERE wypozyczenie = 1";
    try {
      $stmt = $db->prepare($query);
      $stmt->execute();
      $ilosc_sprzetu_wypozyczonego=$stmt->rowCount();
    } catch (PDOException $ex){ die("Failed to run query: " . $ex->getMessage()); }
?>

<header class="header black-bg">
  <div class="sidebar-toggle-box">
    <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
  </div>
  <!--logo start-->
  <a href="index.php" class="logo"><b>Panel administracyjny</b></a>
  <!--logo end-->
  <div class="nav notify-row" id="top_menu">
    <!--  notification start -->
    <ul class="nav top-menu">
      <!-- settings start -->
      <li class="dropdown">
        <a data-toggle="dropdown" class="dropdown-toggle" href="index.php#">
        <i class="fa fa-tasks">ILOŚĆ SPRZĘTU NA WYPOŻYCZENIU</i>
        <span class="badge bg-theme"><?php echo $ilosc_sprzetu_wypozyczonego; ?></span>
        </a>

      </li>
      <!-- settings end -->
      <!-- inbox dropdown start-->
<!--       <li id="header_inbox_bar" class="dropdown">
        <a data-toggle="dropdown" class="dropdown-toggle" href="index.php#">
        <i class="fa fa-tasks">Oczekujące zamówienia</i>
        <span class="badge bg-theme">5</span>
        </a>
      </li> -->
      <!-- inbox dropdown end -->
    </ul>
    <!--  notification end -->
  </div>
  <div class="top-menu">
    <ul class="nav pull-right top-menu">
      <li><a class="logout" href="logout.php">Wyloguj</a></li>
    </ul>
  </div>
</header>
