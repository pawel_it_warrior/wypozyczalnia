<?php
class Baza
{

  function sprawdz_zmiany($bbb) {
    $query = 'SELECT * FROM Var WHERE id=1 LIMIT 1';
    $stmt = $bbb->prepare($query);
    $result = $stmt->execute();
    $r = $stmt->fetch();
    return $r['wartosc'];
  }

  function pobierz_rezerwacje($bbb) {
    $query = '
			SELECT Rezerwacje.id AS id_r, Rezerwacje.id_zamowienia AS id_z, Rezerwacje.numer_rezerwacji AS nr_r,
					Rezerwacje.data_od, Rezerwacje.data_do, Zamowienia.id_customer AS id_user, users.imie, users.nazwisko , users.telefon
				FROM Rezerwacje
				INNER JOIN Zamowienia ON Rezerwacje.id_zamowienia=Zamowienia.id_zamowienia
				INNER JOIN users ON users.id=Zamowienia.id_customer';
    try {
      $stmt = $bbb->prepare($query);
      $result = $stmt->execute();
    }
    catch(PDOException $ex) {
      die("Failed to run query: " . $ex->getMessage());
    }
    $rezerwacje = Array();
    while ($r = $stmt->fetch()) {
      array_push($rezerwacje, $r);
    }
    return $rezerwacje;
  }
  function pobierz_szczegoly_buty($bbb, $id_zamowienia) { // skladniki zamowienia po kliknieciu w szczegoly
    $query = 'SELECT * FROM Skladniki
		INNER JOIN BUTY on Skladniki.id_buty=buty.uid
		INNER JOIN Buty_Producent 	AS Producent 	ON Buty.id_producent=Producent.id
		INNER JOIN Buty_Rozmiar 	AS Rozmiar 		ON Buty.id_rozmiar=Rozmiar.id
		INNER JOIN Buty_Plec 		AS Plec			ON Buty.id_plec=Plec.id
		INNER JOIN Buty_Typ 		AS Typ 			ON Buty.id_typ=Typ.id
		INNER JOIN Buty_Zaawansowanie AS Zaawansowanie ON Buty.id_zaawansowanie=Zaawansowanie.id
		WHERE id_zamowienia = :id_zamowienia';
    $query_params = array(':id_zamowienia' => $id_zamowienia);
    try {
      $stmt = $bbb->prepare($query);
      $result = $stmt->execute($query_params);
    }
    catch(PDOException $ex) {
      die("Failed to run query: " . $ex->getMessage());
    }

    $szczegoly = Array();
    while ($r = $stmt->fetch()) {
      array_push($szczegoly, $r);
    }
    return $szczegoly;
  }
  function pobierz_szczegoly_narty($bbb, $id_zamowienia) { // skladniki zamowienia po kliknieciu w szczegoly
    $query = 'SELECT * FROM Skladniki
		INNER JOIN Narty on skladniki.id_narty=Narty.uid
		INNER JOIN Narty_Producent 	AS Producent 	ON Narty.id_producent=Producent.id
		INNER JOIN Narty_Rozmiar 	AS Rozmiar 		ON Narty.id_rozmiar=Rozmiar.id
		INNER JOIN Narty_Plec 		AS Plec			ON Narty.id_plec=Plec.id
		INNER JOIN Narty_Typ 		AS Typ 			ON Narty.id_typ=Typ.id
		INNER JOIN Narty_Zaawansowanie AS Zaawansowanie ON Narty.id_zaawansowanie=Zaawansowanie.id
		WHERE id_zamowienia = :id_zamowienia';
    $query_params = array(':id_zamowienia' => $id_zamowienia);
    try {
      $stmt = $bbb->prepare($query);
      $result = $stmt->execute($query_params);
    }
    catch(PDOException $ex) {
      die("Failed to run query: " . $ex->getMessage());
    }

    $szczegoly = Array();
    while ($r = $stmt->fetch()) {
      array_push($szczegoly, $r);
    }
    return $szczegoly;
  }
  function pobierz_szczegoly_deski($bbb, $id_zamowienia) { // skladniki zamowienia po kliknieciu w szczegoly
    $query = 'SELECT * FROM Skladniki
		INNER JOIN Deski on skladniki.id_deski=Deski.uid
		INNER JOIN Deski_Producent 	AS Producent 	ON Deski.id_producent=Producent.id
		INNER JOIN Deski_Rozmiar 	AS Rozmiar 		ON Deski.id_rozmiar=Rozmiar.id
		INNER JOIN Deski_Plec 		AS Plec			ON Deski.id_plec=Plec.id
		INNER JOIN Deski_Typ 		AS Typ 			ON Deski.id_typ=Typ.id
		INNER JOIN Deski_Zaawansowanie AS Zaawansowanie ON Deski.id_zaawansowanie=Zaawansowanie.id
		WHERE id_zamowienia = :id_zamowienia';
    $query_params = array(':id_zamowienia' => $id_zamowienia);
    try {
      $stmt = $bbb->prepare($query);
      $result = $stmt->execute($query_params);
    }
    catch(PDOException $ex) {
      die("Failed to run query: " . $ex->getMessage());
    }

    $szczegoly = Array();
    while ($r = $stmt->fetch()) {
      array_push($szczegoly, $r);
    }
    return $szczegoly;
  }

  function zarejestruj_zmiane($bbb) {
    $query = ('UPDATE Var SET wartosc = wartosc + 1 WHERE id=1');
    $stmt = $bbb->prepare($query);
    $stmt->execute();
  }

  function pobierz_klientow($bbb) {
    $query = 'SELECT * FROM users WHERE (role="nobody" OR role="customer") AND (users.usuniety=0) ';
    try {
      $stmt = $bbb->prepare($query);
      $result = $stmt->execute();
    }
    catch(PDOException $ex) {
      die("Failed to run query: " . $ex->getMessage());
    }

    $klienci = Array();
    while ($r = $stmt->fetch()) {
      array_push($klienci, $r);
    }
    return $klienci;
  }

  function pobierz_klienta_do_edycji($bbb, $email) {
    $query = 'SELECT * FROM users WHERE email = :email';
    $query_params = array(':email' => $email);
    try {
      $stmt = $bbb->prepare($query);
      $stmt->execute($query_params);
    }
    catch(PDOException $ex) {
      die("Failed to run query: " . $ex->getMessage());
    }

    $klienci = Array();
    while ($r = $stmt->fetch()) {
      array_push($klienci, $r);
    }
    return $klienci;
  }

  function usun_klienta($bbb, $email) {
    $query = 'UPDATE users SET usuniety = "1" WHERE email = :email';
    $query_params = array(':email' => $email);
    try {
      $stmt = $bbb->prepare($query);
      $stmt->execute($query_params);
    }
    catch(PDOException $ex) {
      die("Failed to run query: " . $ex->getMessage());
    }
  }

  function zapisz_klienta_po_edycji($bbb, $email, $em_stary, $imie, $na, $tel, $pass, $salt) {
    $query = '';
    $query_params = '';
    if (trim($pass) == '' || $pass == null) {
      $query = 'UPDATE users SET imie = :imie, nazwisko = :na, telefon = :tel, email = :email WHERE email = :email_s ';
      $query_params = array(':email' => $email, ':imie' => $imie, ':tel' => $tel, ':na' => $na, ':email_s' => $em_stary);
    }
    else {
      $query = 'UPDATE users SET imie = :imie, nazwisko = :na, telefon = :tel, email = :email, password = :haslo, salt = :salt WHERE email = :email_s ';
      $query_params = array(':email' => $email, ':imie' => $imie, ':tel' => $tel, ':na' => $na, ':email_s' => $em_stary, ':haslo' => $pass, ':salt' => $salt);
    }

    try {
      $stmt = $bbb->prepare($query);
      $stmt->execute($query_params);
    }
    catch(PDOException $ex) {
      die("Failed to run query: " . $ex->getMessage());
      return false;
    }
    return true;
  }

  function sprw_czy_email_istnieje($bbb, $email) {
    $query = 'SELECT * FROM users WHERE email = :email';
    $query_params = array(':email' => $email);
    try {
      $stmt = $bbb->prepare($query);
      $stmt->execute($query_params);
    }
    catch(PDOException $ex) {
      die("Failed to run query: " . $ex->getMessage());
    }

    $row = $stmt->fetchAll();
    if (count($row) > 0) {
      return 'znalazlem'; // znalazlo maila

    }
    else return 'nie znalazlem'; // nie znalazlo maila

  }
  function sprw_czy_user_istnieje($bbb, $user) {
    $query = 'SELECT * FROM users WHERE username = :user';
    $query_params = array(':user' => $user);
    try {
      $stmt = $bbb->prepare($query);
      $stmt->execute($query_params);
    }
    catch(PDOException $ex) {
      die("Failed to run query: " . $ex->getMessage());
    }

    $row = $stmt->fetchAll();
    if (count($row) > 0) {
      return 'znalazlem'; // znalazlo maila

    }
    else return 'nie znalazlem'; // nie znalazlo maila

  }

  function zaszyfruj_haslo($pass, $salt) {
    $password = hash('sha256', $pass . $salt);
    for ($round = 0; $round < 65536; $round++) {
      $password = hash('sha256', $password . $salt);
    }
    return $password;
  }

  function dodaj_klienta($db, $imie, $nazwisko, $email, $telefon) {
    $query = "INSERT INTO users (imie, nazwisko, email, telefon, role) VALUES (:imie, :nazwisko, :email , :telefon,'customer')";
    $query_params = array(':imie' => $imie, ':nazwisko' => $nazwisko, ':email' => $email, ':telefon' => $telefon);
    try {
      $stmt = $db->prepare($query);
      $result = $stmt->execute($query_params);
    }
    catch(PDOException $ex) {
      return false;
    }
    return true;
  }

  function zarejestruj_klienta($db, $username, $password, $salt, $email) {
    $query = 'UPDATE users SET username = :username, password = :password, salt = :salt, role = "customer" WHERE email = :email ';
    $query_params = array(':username' => $username, ':password' => $password, ':salt' => $salt, ':email' => $email);
    //$query ='UPDATE users SET imie = :imie, nazwisko = :na, telefon = :tel, email = :email WHERE email = :email_s ';
    //$query_params = array(':email' => $email, ':imie' => $imie, ':tel' => $tel, ':na' => $na, ':email_s' => $em_stary );

    try {
      $stmt = $db->prepare($query);
      $stmt->execute($query_params);
    }
    catch(PDOException $ex) {
      return false;
    }

    return true;
  }
}
?>