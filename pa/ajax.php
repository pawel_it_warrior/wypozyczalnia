<?php
require ('config.php');

if (isset($_POST['buty_producent_nowy_value'])) {
  $query = "INSERT INTO Buty_Producent (buty_producent_nazwa) VALUES (:nazwa)";
  $query_params = array(":nazwa" => $_POST['buty_producent_nowy_value']);
  try {
    $stmt = $db->prepare($query);
    $result = $stmt->execute($query_params);
    $query = "SELECT * FROM Buty_Producent";
    $stmt = $db->prepare($query);
    $result = $stmt->execute();
    $count = $stmt->rowCount();
    $i = 0;
    $json = '[{ "success" : "true" },';
    while ($r = $stmt->fetch()) {
      if ($i == $count - 1) $json = $json . ' { "producent" : "' . $r['buty_producent_nazwa'] . '" ';
      else $json = $json . '{ "producent" : "' . $r['buty_producent_nazwa'] . '" }, ';
      $i++;
    }
    $json = $json . '}]';
  }
  catch(PDOException $ex) {
    die("Failed to run query: " . $ex->getMessage());
    $json = '[{ "success" : "false"}]';
  }
  echo $json;
}

if (isset($_POST['narty_producent_nowy_value'])) {
  $query = "INSERT INTO Narty_Producent (narty_producent_nazwa) VALUES (:nazwa)";
  $query_params = array(":nazwa" => $_POST['narty_producent_nowy_value']);
  try {
    $stmt = $db->prepare($query);
    $result = $stmt->execute($query_params);
    $query = "SELECT * FROM Narty_Producent";
    $stmt = $db->prepare($query);
    $result = $stmt->execute();
    $count = $stmt->rowCount();
    $i = 0;
    $json = '[{ "success" : "true" },';
    while ($r = $stmt->fetch()) {
      if ($i == $count - 1) $json = $json . ' { "producent" : "' . $r['narty_producent_nazwa'] . '" ';
      else $json = $json . '{ "producent" : "' . $r['narty_producent_nazwa'] . '" }, ';
      $i++;
    }
    $json = $json . '}]';
  }
  catch(PDOException $ex) {
    die("Failed to run query: " . $ex->getMessage());
    $json = '[{ "success" : "false"}]';
  }
  echo $json;
}

if (isset($_POST['deski_producent_nowy_value'])) {
  $query = "INSERT INTO Deski_Producent (deski_producent_nazwa) VALUES (:nazwa)";
  $query_params = array(":nazwa" => $_POST['deski_producent_nowy_value']);
  try {
    $stmt = $db->prepare($query);
    $result = $stmt->execute($query_params);
    $query = "SELECT * FROM Deski_Producent";
    $stmt = $db->prepare($query);
    $result = $stmt->execute();
    $count = $stmt->rowCount();
    $i = 0;
    $json = '[{ "success" : "true" },';
    while ($r = $stmt->fetch()) {
      if ($i == $count - 1) $json = $json . ' { "producent" : "' . $r['deski_producent_nazwa'] . '" ';
      else $json = $json . '{ "producent" : "' . $r['deski_producent_nazwa'] . '" }, ';
      $i++;
    }
    $json = $json . '}]';
  }
  catch(PDOException $ex) {
    die("Failed to run query: " . $ex->getMessage());
    $json = '[{ "success" : "false"}]';
  }
  echo $json;
}

if (isset($_POST['action']) && $_POST['action'] == 'dodaj_sprzet') {
  $query = "SELECT id FROM " . ucfirst($_POST['rodzaj']) . "_Producent WHERE " . $_POST['rodzaj'] . '_producent_nazwa' . " = :wartosc";
  $query_params = array(":wartosc" => $_POST['producent']);
  try {
    $stmt = $db->prepare($query);
    $result = $stmt->execute($query_params);
    $row = $stmt->fetchAll();
    $id_producent = $row[0]['id'];
  }
  catch(PDOException $ex) {
    echo "Failed to run query: " . $ex->getMessage();
  }

  $query = "SELECT id FROM " . ucfirst($_POST['rodzaj']) . "_Typ WHERE " . $_POST['rodzaj'] . '_typ_nazwa' . " = :wartosc";
  $query_params = array(":wartosc" => $_POST['typ']);
  try {
    $stmt = $db->prepare($query);
    $result = $stmt->execute($query_params);
    $row = $stmt->fetchAll();
    $id_typ = $row[0]['id'];
  }
  catch(PDOException $ex) {
    echo "Failed to run query: " . $ex->getMessage();
  }

  $query = "SELECT id FROM " . ucfirst($_POST['rodzaj']) . "_Rozmiar WHERE " . $_POST['rodzaj'] . '_rozmiar' . " = :wartosc";
  $query_params = array(":wartosc" => $_POST['rozmiar']);
  try {
    $stmt = $db->prepare($query);
    $result = $stmt->execute($query_params);
    $row = $stmt->fetchAll();
    $id_rozmiar = $row[0]['id'];
  }
  catch(PDOException $ex) {
    echo "Failed to run query: " . $ex->getMessage();
  }

  $query = "SELECT id FROM " . ucfirst($_POST['rodzaj']) . "_Zaawansowanie WHERE " . $_POST['rodzaj'] . '_zaawansowanie_nazwa' . " = :wartosc";
  $query_params = array(":wartosc" => $_POST['zaawansowanie']);
  try {
    $stmt = $db->prepare($query);
    $result = $stmt->execute($query_params);
    $row = $stmt->fetchAll();
    $id_zaawansowanie = $row[0]['id'];
  }
  catch(PDOException $ex) {
    echo "Failed to run query: " . $ex->getMessage();
  }

  $query = "SELECT id FROM " . ucfirst($_POST['rodzaj']) . "_Plec WHERE " . $_POST['rodzaj'] . '_plec_nazwa' . " = :wartosc";
  $query_params = array(":wartosc" => $_POST['plec']);
  try {
    $stmt = $db->prepare($query);
    $result = $stmt->execute($query_params);
    $row = $stmt->fetchAll();
    $id_plec = $row[0]['id'];
  }
  catch(PDOException $ex) {
    echo "Failed to run query: " . $ex->getMessage();
  }
  if ($_POST['rodzaj'] == 'buty'){
  	$query = "INSERT INTO Buty (id_rozmiar, id_producent, id_typ, id_zaawansowanie, id_plec, buty_model)
  						VALUES (:id_rozmiar, :id_producent, :id_typ, :id_zaawansowanie, :id_plec, :model)";
  }
  if ($_POST['rodzaj'] == 'narty'){
  	$query = "INSERT INTO Narty (id_rozmiar, id_producent, id_typ, id_zaawansowanie, id_plec, narty_model)
  						VALUES (:id_rozmiar, :id_producent, :id_typ, :id_zaawansowanie, :id_plec, :model)";
  }
  if ($_POST['rodzaj'] == 'deski'){
  	$query = "INSERT INTO Deski (id_rozmiar, id_producent, id_typ, id_zaawansowanie, id_plec, deski_model
  						VALUES (:id_rozmiar, :id_producent, :id_typ, :id_zaawansowanie, :id_plec, :model)";
	}
  $query_params = array(":id_rozmiar" => $id_rozmiar,
  											":id_producent" => $id_producent,
  											":id_typ" => $id_typ,
  											":id_zaawansowanie" => $id_zaawansowanie,
  											":id_plec" => $id_plec,
  											":model" => $_POST['model']);
  try {
    $stmt = $db->prepare($query);
    $result = $stmt->execute($query_params);
 		echo '[{ "success" : "true"}]';
  }
  catch(PDOException $ex) {
    echo '[{ "success" : "false"}]';
    echo "Failed to run query: " . $ex->getMessage();
  }
}

if (isset($_POST['action']) && $_POST['action'] == 'pull') {
  $queryButy = "SELECT * FROM Buty
					INNER JOIN Buty_Producent 	AS Producent 	ON Buty.id_producent=Producent.id
					INNER JOIN Buty_Rozmiar 	AS Rozmiar 		ON Buty.id_rozmiar=Rozmiar.id
					INNER JOIN Buty_Plec 		AS Plec			ON Buty.id_plec=Plec.id
					INNER JOIN Buty_Typ 		AS Typ 			ON Buty.id_typ=Typ.id
					INNER JOIN Buty_Zaawansowanie AS Zaawansowanie ON Buty.id_zaawansowanie=Zaawansowanie.id
          WHERE Buty.widocznosc=1";
  $queryNarty = "SELECT * FROM Narty
					INNER JOIN Narty_Producent 	AS Producent 	ON Narty.id_producent=Producent.id
					INNER JOIN Narty_Rozmiar 	AS Rozmiar 		ON Narty.id_rozmiar=Rozmiar.id
					INNER JOIN Narty_Plec 		AS Plec			ON Narty.id_plec=Plec.id
					INNER JOIN Narty_Typ 		AS Typ 			ON Narty.id_typ=Typ.id
					INNER JOIN Narty_Zaawansowanie AS Zaawansowanie ON Narty.id_zaawansowanie=Zaawansowanie.id
          WHERE Narty.widocznosc=1";
  $queryDeski = "SELECT * FROM Deski
					INNER JOIN Deski_Producent 	AS Producent 	ON Deski.id_producent=Producent.id
					INNER JOIN Deski_Rozmiar 	AS Rozmiar 		ON Deski.id_rozmiar=Rozmiar.id
					INNER JOIN Deski_Plec 		AS Plec			ON Deski.id_plec=Plec.id
					INNER JOIN Deski_Typ 		AS Typ 			ON Deski.id_typ=Typ.id
					INNER JOIN Deski_Zaawansowanie AS Zaawansowanie ON Deski.id_zaawansowanie=Zaawansowanie.id
          WHERE Deski.widocznosc=1";
  try {
    $stmtButy = $db->prepare($queryButy);
    $result = $stmtButy->execute();
    $stmtNarty = $db->prepare($queryNarty);
    $result = $stmtNarty->execute();
    $stmtDeski = $db->prepare($queryDeski);
    $result = $stmtDeski->execute();
  }
  catch(PDOException $ex) {
    die("Failed to run query: " . $ex->getMessage());
  }

  $items = Array();
  while ($r = $stmtNarty->fetch()) {
    $r["rodzaj"] = "narty";
    array_push($items, $r);
  }
  while ($r = $stmtButy->fetch()) {
    $r["rodzaj"] = "buty";
    array_push($items, $r);
  }
  while ($r = $stmtDeski->fetch()) {
    $r["rodzaj"] = "deski";
    array_push($items, $r);
  }
  echo json_encode($items, JSON_UNESCAPED_UNICODE);
}


if (isset($_POST['action']) && $_POST['action'] == 'usun_sprzet'){
  $uid = $_POST['uid'];
  $rodzaj = $_POST['rodzaj'];
  if ($rodzaj == 'buty'){
    $sql = "UPDATE Buty SET widocznosc=0 WHERE uid=$uid";
  } else if ($rodzaj == 'narty'){
    $sql = "UPDATE Narty SET widocznosc=0 WHERE uid=$uid";
  } else if ($rodzaj == 'deski'){
    $sql = "UPDATE Deski SET widocznosc=0 WHERE uid=$uid";
  }
  $stmt = $db->prepare($sql);
  $stmt->execute();
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
if (isset($_POST['action']) && $_POST['action'] == 'zaakceptuj_rezerwacje'){

  $zamowienie = $_POST['zamowienie'];
  $q = "SELECT id_buty, id_deski, id_narty FROM Skladniki WHERE Skladniki.id_zamowienia = $zamowienie";
  $stmt = $db->prepare($q);
  $stmt->execute();
  $uid_buty = Array();
  $uid_deski = Array();
  $uid_narty = Array();
  while ($r = $stmt->fetch()) {
    if ($r['id_buty'] != NULL) {
      array_push($uid_buty, $r['id_buty']);
    }
    if ($r['id_deski'] != NULL) {
      array_push($uid_deski, $r['id_deski']);
    }
    if ($r['id_narty'] != NULL) {
      array_push($uid_narty, $r['id_narty']);
    }
  }
  if ( !empty($uid_buty) ){
    $uid_buty = "'"  . implode($uid_buty, "', '") . "'";
    $sql = "UPDATE Buty SET wypozyczony=1 WHERE id IN ( $uid_buty )";
    $stmt = $db->prepare($sql);
    $stmt->execute();
  }
  if ( !empty($uid_deski) ){
    $uid_deski = "'" . implode($uid_deski, "', '") . "'";
    $sql = "UPDATE Deski SET wypozyczony=1 WHERE id IN ( $uid_deski )";
    $stmt = $db->prepare($sql);
    $stmt->execute();
  }
  if ( !empty($uid_narty) ){
    $uid_narty = "'" . implode($uid_narty, "', '") . "'";
    $sql = "UPDATE Narty SET wypozyczony=1 WHERE id IN ( $uid_narty )";
    $stmt = $db->prepare($sql);
    $stmt->execute();
  }


}
?>