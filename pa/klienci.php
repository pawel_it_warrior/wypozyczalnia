<?php
  ini_set( 'display_errors', 'On' );
  error_reporting( E_ALL );
  require("config.php");
  if (empty($_SESSION['user']['username'])){
    header("Location: login.php");
  } else if ($_SESSION['user']['role'] === 'admin') {

?>
<!DOCTYPE html>
<html lang="pl">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>PANEL ADMINISTRACYJNY</title>
  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="assets/font-awesome/css/font-awesome.css"  />
  <link rel="stylesheet" type="text/css" href="assets/js/gritter/css/jquery.gritter.css" />
  <link rel="stylesheet" type="text/css" href="assets/lineicons/style.css">
  <link rel="stylesheet" type="text/css" href="assets/css/style.css">
  <link rel="stylesheet" type="text/css" href="assets/css/style-responsive.css">
  <style type="text/css">
    button.generuj:hover { background-color: #68dff0; }
  </style>
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body>
  <section id="container" >

    <?php include("header.php"); ?>

    <!-- MAIN SIDEBAR MENU -->

    <?php include("sidebar.php"); ?>

    <!-- MAIN CONTENT -->

  <!--main content start-->
  <section id="main-content">
    <section class="wrapper">

    <div class="col-lg-12 main-chart">
			<div class="col-md-12">

				<h4>
				  <button type="button" id="button_customer_add" class="btn btn-theme">Dodaj klienta</button>
				  <button type="button" id="button_customer_register" class="btn btn-theme04" customer_mail_register="">Zarejestruj klienta</button>
				  <button type="button" id="button_customer_delete" class="btn btn-theme04" customer_mail_delete="">Usuń klienta</button>
				  <button type="button" id="button_customer_edit" class="btn btn-theme04" customer_mail_edit="">Edytuj Klient klienta</button>
				</h4>

			<!-- TABELA -->
			  <div class="content-panel" style="padding-bottom: 0px; padding-top: 0px;">
				<table class="table" style="margin-bottom: 0px;">
				<thead>
					<tr style="background: #ffd777; color: #000;" id="tonie">
						<th>Imie</th>
						<th>Nazwisko</th>
						<th>Email</th>
						<th>Nr telefonu</th>
						<th>username</th>
					</tr>
				</thead>
				<tbody class="searchable" id="items">
				</tbody>
					</table>
			  </div>
			<!-- /TABELA -->

		<!-- REGISTERMODAL POPUP -->
          <div class="modal fade" id="customerRegisterModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title" id="myModalLabelRegister">Zarejestruj klienta</h4>
                </div>

                <!-- content -->
                <div class="modal-body" id="modal-body-content">
								<div id="content">
									<form id="customer-register-form" class="form-horizontal" action="" method="POST">
										<fieldset>
											<div class="form-group">
												<label class="col-md-4 control-label" for="register_username">Imię *</label>
												<div class="col-md-4">
													<input id="register_username" name="register_username" type="text" placeholder="Nazwa użytkownika" class="form-control input-md" maxlength="30" required>
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-4 control-label" for="register_password">Nazwisko *</label>
												<div class="col-md-4">
													<input id="register_password" name="register_password" type="text" placeholder="Hasło" class="form-control input-md" required>
												</div>
											</div>
										</fieldset>
										<div class="form-group" >
											<div class="col-md-12" align="center">
												<input type="hidden" name="rejestracja" value="1">
												<input type="hidden" name="register_email" id="register_email" value="">
												<input id="customerRegister" type="submit" name="customerRegister" class="btn btn-info" align="center" style="width: 30%;" value="Zarejestruj" />
											</div>
										</div>
										<div class="form-group">
											<div class="col-md-12" align="center">
												<input type="reset" class="btn" align="center" style="width: 30%;" value="Reset" />
											</div>
										</div>
										<div class="form-group">
											<div class="col-md-12" align="center">
												<span id="register_msg"></span>
											</div>
										</div>
									</form>
								</div>

                </div>
                <!-- /content -->

                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
                </div>

              </div>
            </div>
          </div>
          <!-- /MODAL POPUP -->

         <!-- ADD MODAL POPUP -->
          <div class="modal fade" id="customerAddModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title" id="myModalLabel">Dodaj nowego klienta</h4>
                </div>

                <!-- content -->
                <div class="modal-body" id="modal-body-content">
								<div id="content">
									<form id="customer-add-form" class="form-horizontal" action="" method="POST">
										<fieldset>
											<div class="form-group">
												<label class="col-md-4 control-label" for="add_imie">Imię *</label>
												<div class="col-md-4">
													<input id="add_imie" name="add_imie" type="text" placeholder="Imię" class="form-control input-md" maxlength="30" required>
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-4 control-label" for="add_nazwisko">Nazwisko *</label>
												<div class="col-md-4">
													<input id="add_nazwisko" name="add_nazwisko" type="text" placeholder="Nazwisko" class="form-control input-md" required>
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-4 control-label" for="add_email">Email *</label>
												<div class="col-md-4">
													<input id="add_email" name="add_email" type="text" placeholder="Email" class="form-control input-md" required>
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-4 control-label" for="add_telefon">Telefon</label>
												<div class="col-md-4">
													<input id="add_telefon" name="add_telefon" type="text" placeholder="Telefon" class="form-control input-md">
												</div>
											</div>
										</fieldset>
										<div class="form-group" >
											<div class="col-md-12" align="center">
												<input type="hidden" name="dodawanie" value="1">
												<input id="customerAddToDatabase" type="submit" name="customerAddToDatabase" class="btn btn-info" align="center" style="width: 30%;" value="Dodaj" />
											</div>
										</div>
										<div class="form-group">
											<div class="col-md-12" align="center">
												<input type="reset" class="btn" align="center" style="width: 30%;" value="Reset" />
											</div>
										</div>
										<div class="form-group">
											<div class="col-md-12" align="center">
												<span id="add_msg"></span>
											</div>
										</div>
									</form>
								</div>

                </div>
                <!-- /content -->

                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
                </div>

              </div>
            </div>
          </div>
          <!-- /MODAL POPUP -->

		   <!-- DELETE MODAL POPUP -->
          <div class="modal fade" id="customerDeleteModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title" id="myModalLabelDelete">Potwierdź usunięcie klienta</h4>
                </div>

                <!-- content -->
                <div class="modal-body" id="modal-body-content">
								<div id="content">
									<form id="customer-delete-form" class="form-horizontal" action="" method="POST">
										<div class="form-group" >
											<div class="col-md-12" align="center">
												<input type="hidden" name="usuwanie" value="1">
												<input id="customerDeleteFromDatabase" type="submit" name="customerDeleteFromDatabase" class="btn btn-theme04" align="center" style="width: 30%;" value="USUŃ" />
											</div>
										</div>
										<div class="form-group">
											<div class="col-md-12" align="center">
												<span id="delete_msg"></span>
											</div>
										</div>
									</form>
								</div>

                </div>
                <!-- /content -->

                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
                </div>

              </div>
            </div>
          </div>
          <!-- /MODAL POPUP -->


		   <!-- EDIT MODAL POPUP -->
          <div class="modal fade" id="customerEditModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" >
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title" id="myModalLabelEdit">Edytuj klienta</h4>
                </div>

                <!-- content -->
                <div class="modal-body" id="modal-body-content">
								<div id="content">
									<form id="customer-edit-form" class="form-horizontal" action="" method="POST">
										<fieldset>
											<div class="form-group">
												<label class="col-md-4 control-label" for="edit_imie">Imię *</label>
												<div class="col-md-4">
													<input id="edit_imie" name="edit_imie" type="text" placeholder="Imię" class="form-control input-md" maxlength="30" required>
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-4 control-label" for="edit_nazwisko">Nazwisko *</label>
												<div class="col-md-4">
													<input id="edit_nazwisko" name="edit_nazwisko" type="text" placeholder="Nazwisko" class="form-control input-md" required>
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-4 control-label" for="edit_haslo">Hasło *</label>
												<div class="col-md-4">
													<input id="edit_haslo" name="edit_haslo" type="text" placeholder="Hasło" class="form-control input-md" >
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-4 control-label" for="edit_email">Email *</label>
												<div class="col-md-4">
													<input id="edit_email" name="edit_email" type="text" placeholder="Email" class="form-control input-md" >
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-4 control-label" for="edit_telefon">Telefon</label>
												<div class="col-md-4">
													<input id="edit_telefon" name="edit_telefon" type="text" placeholder="Telefon" class="form-control input-md"  >
												</div>
											</div>
										</fieldset>
										<div class="form-group" >
											<div class="col-md-12" align="center">
												<input type="hidden" name="edycja" value="1">
												<input type="hidden" name="edit_email_old" id="edit_email_old" value="">
												<input type="hidden" name="edit_salt" id="edit_salt" value="">
												<input id="customerEditInDatabase" type="submit" name="customerEditInDatabase" class="btn btn-info" align="center" style="width: 30%;" value="Edytuj" />
											</div>
										</div>
										<div class="form-group">
											<div class="col-md-12" align="center">
												<input type="reset" class="btn" align="center" style="width: 30%;" value="Reset" />
											</div>
										</div>
										<div class="form-group">
											<div class="col-md-12" align="center">
												<span id="edit_msg"></span>
											</div>
										</div>
									</form>
								</div>

                </div>
                <!-- /content -->

                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
                </div>

              </div>
            </div>
          </div>
          <!-- /MODAL POPUP -->

			</div>
		</div>
  </section>
  </section>
  <!--main content end-->

  <!--footer start-->
  <?php include("footer.php"); ?>
  <!--footer end-->


  </section>
  <script src="assets/js/jquery.js"></script>
  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>
  <script src="assets/js/jquery.scrollTo.min.js"></script>
  <script src="assets/js/jquery.nicescroll.js"></script>
  <script src="assets/js/jquery.sparkline.js"></script>
  <script src="assets/js/klienci.js"></script>
  <script src="assets/js/jquery.validate.js"></script>
	<script>
	// $(function(){
		// var s= document.getElementById('items').getAttribute('licznik');
		// alert('agad');
		// console.log(s);
	// });
	$.ajax({
		url: 'checker.php',
		type: 'post',
		dataType : 'json',
		data: { akcja_klienci_pobierz : 'pobierz_klientow'},
		success: function (data) {
			var str = '';
			    str += '<tbody  class="searchable" id="items">';
				for (var i = 0; i < data.length ; i++) {
				str += '<tr style="color: rgb(0, 0, 0); display: table-row; background: rgb(104, 223, 240);" customermail="'+ data[i].email +'" customerrole="'+ data[i].role +'">';
				str += '<td>'+ data[i].imie +'</td>';
				str += '<td>'+ data[i].nazwisko +'</td>';
				str += '<td>'+ data[i].email +'</td>';
				str += '<td>'+ data[i].telefon +'</td>';
				if (data[i].username == null)
				{
					str += '<td>Niezarejestrowany</td>';
				}
				else
				{
					str += '<td>'+ data[i].username +'</td>';
				}
				str += '</tr>';
			  };
			  str += '</tbody>';
			  $('#items').replaceWith(str);
		}
	});

  // INNE ZDARZENIA
  // DODAJ KLIENTA
   $('body').on('click', value='#button_customer_add' , function() {
	var options = {
      "backdrop" : "static",
      "keyboard" : "true"
    }
    $('#customerAddModal').modal(options);
   });


  // USUN KLIENTA
   $('body').on('click', value='#button_customer_delete' , function() {
	    if ( $('tr').hasClass( "active" ) ) {
			var x = $(this).attr("customer_mail_delete");
			$('#myModalLabelDelete').text('Potwierdź usunięcie - '+x);
			var options = {
			  "backdrop" : "static",
			  "keyboard" : "true"
			}
			$('#customerDeleteModal').modal(options);
	    }
		else
		{
			alert('Wybierz wiersz tabeli');
		}
   });

   $('body').on('click', value='#customerDeleteFromDatabase', function() {
	   var e = document.getElementById('button_customer_delete').getAttribute('customer_mail_delete');
	   $.ajax({
		url: 'checker.php',
		type: 'post',
		dataType : 'json',
		data: { akcja_klienci_usun : 'usun_klienta', email : e},
		success: function (data) {
			alert(e);
			//console.log(data);
		}
	});
   });

   // EDYTUJ KLIENTA
   $('body').on('click', value='#button_customer_edit' , function() {
	   if ( $('tr').hasClass( "active" ) ) {
			var x = $(this).attr("customer_mail_edit");

			var options = {
			  "backdrop" : "static",
			  "keyboard" : "true"
			}
			$('#customerEditModal').modal(options);
			$.ajax({
				url: 'checker.php',
				type: 'post',
				dataType : 'json',
				data: { akcja_pobierz_klienta_do_edycji : 'pobierz', email : x},
				success: function (data) {
					$('#edit_imie').val(data[0].imie);
					$('#edit_nazwisko').val(data[0].nazwisko);
					//$('#edit_haslo').val(data[0].haslo);
					$('#edit_telefon').val(data[0].telefon);
					$('#edit_email').val(data[0].email);
					$('#edit_salt').val(data[0].salt);
				}
			});
	    }
		else
		{
			alert('Wybierz wiersz tabeli');
		}
   });
   // ZAREJESTRUJ KLIENTA
    $('body').on('click', value='#button_customer_register' , function() {
	    if ( $('tr').hasClass( "active" ) )
		{
			var x = $(this).attr("customer_mail_register");
			var y = $('.active').attr('customerrole');
			if(y == "customer")
			{
				$('#myModalLabelRegister').text('Potwierdź usunięcie - '+x);
				var options = {
				  "backdrop" : "static",
				  "keyboard" : "true"
				}
				$('#customerRegisterModal').modal(options);
			}
			else
			{
				alert('Już zarejestrowany');
			}
	    }
		else
		{
			alert('Wybierz wiersz tabeli');
		}
   });

  $('body').on('click', ".table tr", function() {
    if ($(this).attr("id") != "tonie"){
      $(".table tr").each(function () {
        $(this).attr("class", "");
      });
        $(this).attr("class", "active");

		if ( $(this).hasClass( "active" ) ) {
			var x = $(this).attr("customermail");
			document.getElementById('button_customer_delete').setAttribute('customer_mail_delete',x);
			document.getElementById('button_customer_edit').setAttribute('customer_mail_edit',x);
			document.getElementById('button_customer_register').setAttribute('customer_mail_register',x);
			$("#edit_email_old").val(x);
			$("#register_email").val(x);
		}

    }

  });

	</script>
</body>
</html>
<?php } ?>