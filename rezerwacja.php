<?php
	require("config.php");
	if (isset($_SESSION['koszyk']))
		unset($_SESSION['koszyk']);
?>
<!DOCTYPE html>
<html lang="pl">
	<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Rezerwacja</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="css/animate.css" />
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<link rel="stylesheet" type="text/css" href="css/jquery.datetimepicker.css"/>
	<link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="color/default.css" />
	<style type="text/css">
  .highlighted, tr.highlighted {
    background-color: #5bc0de;
    color: black;
	}
	.table-striped>tbody>tr:nth-child(odd)>th {
	}
	.table-striped>tbody>tr:nth-child(odd)>td {
	}
	td:first-letter {
	    text-transform: uppercase;
	}
	</style>
</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-custom">

<div id="preloader">
	<div id="load"></div>
</div>
<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
	<div class="container">
		<div class="navbar-header page-scroll">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
				<i class="fa fa-bars"></i>
			</button>
			<a class="navbar-brand" href="index.php"> <h1>TOBI SKI</h1> </a>
		</div>
		<div class="collapse navbar-collapse navbar-right navbar-main-collapse">
			<ul class="nav navbar-nav">
				<li><a href="index.php">Strona główna</a></li>
			</ul>
		</div>
	</div>
</nav>
<section id="rezerwacja" class="home-section text-center">
	<div class="heading-rezerwacja">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2">
					<div class="wow bounceInDown" data-wow-delay="0.4s">
						<div class="section-heading">
							<h2>Rezerwacja</h2>
							<i class="fa fa-2x fa-angle-down"></i>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container" id="rezerwacja" >
		<div class="jumbotron" style="background-color: rgba(0,0,0,0.0);">
			<div class="row">
			<div class="col-md-12" align="justify">
			<div class="panel panel-default" >
				<div class="panel-body">

						<legend>Znajdź i zarezerwuj sprzęt dla siebie</legend>
						<div class="panel panel-default">
							<div class="panel-heading">
								Wprowadź swoje parametry i wybierz sprzęt, który chcesz zarezerwować
							</div>
							<div class="panel-body">

								<div class="form-group">
									<div class="col-md-12">
										<div class="col-md-4">Podaj swoją wagę
											<input id="waga" name="waga" type="number" min="20" max="250" placeholder="Waga" class="form-control input-md" required autofocus>
										</div>
										<div class="col-md-4">Podaj swój wzrost
											<input id="wzrost" name="wzrost" type="number" min="60" max="250"placeholder="Wzrost" class="form-control input-md" required autofocus>
										</div>
										<div class="col-md-4">Podaj rozmiar buta (EUR)
											<input id="rozmiarnogi" name="rozmiarnogi" type="number" min="22" max="29" step="0.5" placeholder="Rozmiar nogi/buta" class="form-control input-md" required autofocus>
										</div>
									</div>
									<div class="col-md-12" style="margin-top: 20px;">
		                <div class="col-md-4" >
		                Data odbioru
		                  <input type="text" value="" name="data_od" id="data_od" class="form-control input-md" required="" placeholder="Wybierz datę" />
		                </div>

		                <div class="col-md-4" >
		                Data zwrotu
		                  <input type="text" value="" name="data_do" id="data_do" class="form-control input-md" required="" placeholder="Wybierz datę" />
		                </div>
	                </div>

								</div>
							</div>
							</div>
							<div class="input-group"> <span class="input-group-addon">Filtruj</span>
				        <select id="filter" class="form-control">
				        	<option value=""></option>
					        <option value="Buty">Buty</option>
					        <option value="Narty">Narty</option>
					        <option value="Deski">Deski</option>
						    </select>

							</div>

							<table class="table table-striped" id="tabela">
						    <thead>
					        <tr id="tonie">
				            <th>Rodzaj sprzętu</th>
				            <th>Model</th>
				            <th>Producent</th>
				            <th>Typ</th>
				            <th>Zaawansowanie</th>
				            <th>Płeć</th>
				            <th style="display: none">Rozmiar</th>
				            <th style="display: none">UID</th>
				            <th>Zarezerwuj</th>
					        </tr>
						    </thead>
						    <tbody class="searchable" id="items">

						    </tbody>
							</table>

						<div class="row-fluid">
					    <div class="span6">
				        <div class="pull-right">
			            <p style="font-weight: bold; margin-right: 50px;">KOSZYK: <span id="ilosctowarow">0</span> towarów</p>
			            <button class="btn btn-info form-control" id="dalej">DALEJ</button>
				        </div>
					    </div>
						</div>
							</div>
						</div>


			</div>
			</div>
		</div>
	</div>

</section>

<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-lg-12">
				<p>&copy;Copyright 2014 - Squad. All rights reserved.</p>
			</div>
		</div>
	</div>
</footer>

<form style="display: hidden" action="order.php" method="POST" id="redirect_to_order">
  <input type="hidden" name="redirect_hash" value="1"/>
</form>
<script src="js/jquery.min.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/jquery.scrollTo.js"></script>
<script src="js/rezerwacja.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/custom.js"></script>
<script src="js/jquery.validate.js"></script>
<script src="js/jquery.simplePagination.js"></script>
<script src="js/jquery.datetimepicker.js"></script>
<script>

  // tak, wiem...
  // tu coś lipnie działa, bo mimo tego wspaniałego kodu nadal można wybierać błędne daty ;)

  // może Tobiasz to zauważy i poprawi.

  $(window).bind("pageshow", function() {
    $('#waga').val('');
    $('#wzrost').val('');
    $('#rozmiarnogi').val('');
    $('#data_od').val('');
    $('#data_do').val('');
  });

	var orders = [];

  var startdate = $("#data_od").datetimepicker({
    lang:'pl',
    minDate: 'date',
    useCurrent: true,
    beforeShow: function(){
      $("#data_od").datetimepicker("option", {
        maxDate: $("#data_do").datetimepicker('getDate')
      });
    }
  });

  var enddate = $("#data_do").datetimepicker({
    lang:'pl',
    minDate: 'date',
    useCurrent: true,
    beforeShow: function(){
      $("#data_do").datetimepicker("option", {
        minDate: $("#data_od").datetimepicker('getDate')
      });
    }
  });

	$('body').on('click', "[value*='ok_']", function() {
	 	var id = $(this).attr('value').replace("ok_","");
		$('#'+id).addClass('highlighted');
		if ( $.inArray(id, orders)  == -1 ) {
			orders.push(id);
		}
    $('#ilosctowarow').text( orders.length );
	});

	$('body').on('click', "[value*='no_']", function() {
	 	var id = $(this).attr('value').replace("no_","");
	 	$('#'+id).removeClass('highlighted');
	 	if ( $.inArray(id, orders) != -1 ) {
    	orders = jQuery.grep(orders, function(value) {
        return value != id;
      });
	 	}
    $('#ilosctowarow').text(orders.length);
	 });

  $('#dalej').click(function (){
  	if ( orders.length > 0 ){
  		var my_order = [];
  		for ( var i = 0; i <= orders.length - 1; i++ ) {
  			var row = $('#'+orders[i]).closest("tr");
		    var tableData = row.children("td").map(function() {
	        return $(this).text();
		    }).get();
		    my_order.push(tableData);
  		};
  		var odkiedy = $('#data_od').val();
  		var dokiedy = $('#data_do').val();
	    $.ajax({
		    type:"POST",
		    url:"dbactions.php",
		    data:{ action: "koszyk", zamowienie: my_order, data_od: odkiedy, data_do: dokiedy },
		    dataType: 'text',
	      success:function(data){
	        $('#redirect_to_order').submit();
        }
			});
  	}
  });
</script>
</body>
</html>