<?php
	require("config.php");
	if (!isset($_POST['redirect_hash']) && $_POST['redirect_hash'] != '1' ){
		header("Location: index.php");
		die();
	} else {
?>
<!DOCTYPE html>
<html lang="pl">
	<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Rezerwacja</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="css/animate.css" />
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<link rel="stylesheet" type="text/css" href="css/jquery.datetimepicker.css"/>
	<link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="color/default.css" />
</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-custom">

<div id="preloader">
	<div id="load"></div>
</div>
<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
	<div class="container">
		<div class="navbar-header page-scroll">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
				<i class="fa fa-bars"></i>
			</button>
			<a class="navbar-brand" href="index.php"> <h1>TOBI SKI</h1> </a>
		</div>
		<div class="collapse navbar-collapse navbar-right navbar-main-collapse">
			<ul class="nav navbar-nav">
				<li><a href="index.php">Strona główna</a></li>
			</ul>
		</div>
	</div>
</nav>
<section id="rezerwacja" class="home-section text-center">
	<div class="heading-rezerwacja">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2">
					<div class="wow bounceInDown" data-wow-delay="0.4s">
						<div class="section-heading">
							<h2>Rezerwacja</h2>
							<i class="fa fa-2x fa-angle-down"></i>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container" id="rezerwacja" >
		<div class="jumbotron" style="background-color: rgba(0,0,0,0.0);">
			<div class="row">
			<div class="col-md-12" align="justify">
			<div class="panel panel-default" >
				<div class="panel-body">
					<legend>Wprowadź swoje dane</legend>
					<form id="formularz_rezerwacji" class="form-horizontal" method="POST">
					<div class="row-fluid">

					<?php
					if ( !$_SESSION["logged"] == true ) {
						echo '
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	          <!-- imie -->
	          <div class="form-group">
	            <label class="col-md-4 control-label" for="imie" style="font-size: 80%;">Imię *</label>
	            <div class="col-md-4">
	              <input id="imie" name="imie" type="text" placeholder="Imię" class="form-control input-md" required="" >
	            </div>
	          </div>
	          <!-- nazwisko -->
	          <div class="form-group">
	            <label class="col-md-4 control-label" for="nazwisko" style="font-size: 80%;">Nazwisko *</label>
	            <div class="col-md-4">
	              <input id="nazwisko" name="nazwisko" type="text" placeholder="Nazwisko" class="form-control input-md" required="" >
	            </div>
	          </div>
	          <!-- email -->
	          <div class="form-group">
	            <label class="col-md-4 control-label" for="email" style="font-size: 80%;">Email *</label>
	            <div class="col-md-4">
	              <input id="email" name="email" type="text" placeholder="Email" class="form-control input-md" required="" >
	            </div>
	          </div>
	          <!--  telefon -->
	          <div class="form-group">
	            <label class="col-md-4 control-label" for="telefon" style="font-size: 80%;">Telefon</label>
	            <div class="col-md-4">
	              <input id="telefon" name="telefon" type="telefon" placeholder="Telefon" class="form-control input-md">
	            </div>
	          </div>
	          <!-- agreement -->
	          <div class="form-group">
	            <label class="col-md-4 control-label" for="agree">Wyrażam zgodę na przetwarzanie moich danych osobowych *</label>
	            <div class="col-md-4"><br>
	              <input id="agree" class="checkbox" type="checkbox" name="agree">
	              <input type="hidden" id="zalogowany_user" name="zalogowany_user" value="0">
	            </div>
	          </div>
					</div>';
					} else {
						echo '<input type="hidden" id="zalogowany_user" name="zalogowany_user" value="1">';
						} ?>

          <!-- TABELA -->
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<legend>Twój koszyk</legend>
	          <div class="table-responsive">
	            <table class="table table-bordered">
	              <thead>
	                <tr style="background: #5bc0de; color: #000; ">
	                  <th style="display: none;" >#</th>
	                  <th style="text-align: center;">Model</th>
	                  <th style="text-align: center;">Producent</th>
	                  <th style="text-align: center;">Typ</th>
	                  <th style="text-align: center;">Zaawansowanie</th>
	                  <th style="text-align: center;">Płeć</th>
	                  <th style="text-align: center;">Rozmiar</th>
	                  <th style="display: none;" >UID</th>
	                </tr>
	              </thead>
	              <tbody id="items">
	     					<?php
	     					foreach ($_SESSION['koszyk'] as $kosz) {
	     						if (is_array($kosz)){
	   							echo '<tr style="border-bottom: 1px solid #000; font-size: 80%; font-weight: bold;">';
									echo '<td>'.$kosz[1].'</td>';
									echo '<td>'.$kosz[2].'</td>';
									echo '<td>'.$kosz[3].'</td>';
									echo '<td>'.$kosz[4].'</td>';
									echo '<td>'.$kosz[5].'</td>';
									echo '<td style="text-align: center;">'.$kosz[6].'</td>';
									echo '<td style="display: none;">'.$kosz[7].'</td>';
									echo '<tr>';}}
								?>
	              </tbody>
	            </table>
        		</div>
	          <div class="form-group" >
	            <div class="col-md-12" align="center">
	            	<input type="hidden" name="rezerwacja" value="1">
	              <input id="rezerwuj_submit" type="submit" class="btn btn-info" align="center" style="width: 30%; " value="Rezerwuj" />
	            </div>
	          </div>
	          <div class="form-group">
	            <div class="col-md-12" align="center">
	              <input type="reset" class="btn" align="center" style="width: 30%;" value="Anuluj" id="anuluj" />
	            </div>
	          </div>
	          <div class="form-group">
	            <div class="col-md-12" align="center">
	              <span id="msg"></span>
	            </div>
	          </div>
					</div>
					</div>
          <!-- /TABELA -->

	        </form>
				</div>
		</div>
		</div>
		</div>
	</div>

</section>

<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-lg-12">
				<p>&copy;Copyright 2014 - Squad. All rights reserved.</p>
			</div>
		</div>
	</div>
</footer>

<script src="js/jquery.min.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/jquery.scrollTo.js"></script>
<script src="js/rezerwacja.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/custom.js"></script>
<script src="js/jquery.validate.js"></script>

<?php } ?>
</body>
</html>