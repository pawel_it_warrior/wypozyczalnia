<?php
require ("config.php");
// require 'PHPMailerAutoload.php';
// $mail = new PHPMailer;
// //$mail->SMTPDebug = 3;                               // Enable verbose debug output
// $mail->isSMTP();                                      // Set mailer to use SMTP
// $mail->Host = 'smtp1.example.com;smtp2.example.com';  // Specify main and backup SMTP servers
// $mail->SMTPAuth = true;                               // Enable SMTP authentication
// $mail->Username = 'user@example.com';                 // SMTP username
// $mail->Password = 'secret';                           // SMTP password
// $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
// $mail->Port = 587;                                    // TCP port to connect to

// $mail->From = 'from@example.com';
// $mail->FromName = 'Mailer';
// $mail->addAddress('joe@example.net', 'Joe User');     // Add a recipient
// $mail->addAddress('ellen@example.com');               // Name is optional
// $mail->addReplyTo('info@example.com', 'Information');
// $mail->addCC('cc@example.com');
// $mail->addBCC('bcc@example.com');

// $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
// $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
// $mail->isHTML(true);                                  // Set email format to HTML

// $mail->Subject = 'Here is the subject';
// $mail->Body    = 'This is the HTML message body <b>in bold!</b>';
// $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

// if(!$mail->send()) {
//     echo 'Message could not be sent.';
//     echo 'Mailer Error: ' . $mail->ErrorInfo;
// } else {
//     echo 'Message has been sent';
// }
?>
<!DOCTYPE html>
<html lang="pl">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Kontakt</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="css/animate.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet">
    <link href="color/default.css" rel="stylesheet">

  </head>
  <body id="page-top" data-spy="scroll" data-target=".navbar-custom">
    <div id="preloader">
      <div id="load"></div>
    </div>
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header page-scroll">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
          <i class="fa fa-bars"></i>
          </button>
          <a class="navbar-brand" href="index.php">
            <h1>TOBI Ski</h1>
          </a>
        </div>
        <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
          <ul class="nav navbar-nav">
            <li><a href="index.php">Strona główna</a></li>
          </ul>
        </div>
      </div>
    </nav>

    <section id="contact" class="home-section text-center">
      <div class="heading-contact">
        <div class="container">
          <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
              <div class="wow bounceInDown" data-wow-delay="0.4s">
                <div class="section-heading">
                  <h2>Napisz do nas</h2>
                  <i class="fa fa-2x fa-angle-down"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-lg-2 col-lg-offset-5">
            <hr class="marginbot-50">
          </div>
        </div>
        <div class="row">
          <div class="col-lg-8">
            <div class="boxed-grey">
              <form id="contact-form">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="name">
                      Twoje imię</label>
                      <input type="text" class="form-control" id="name" placeholder="Enter name" required="required" />
                    </div>
                    <div class="form-group">
                      <label for="email">
                      Adres email</label>
                      <div class="input-group">
                        <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span>
                        </span>
                        <input type="email" class="form-control" id="email" placeholder="Enter email" required="required" />
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="subject">
                      Temat</label>
                      <select id="subject" name="subject" class="form-control" required="required">
                        <option value="na" selected="">Wybierz:</option>
                        <option value="oferta">Oferta</option>
                        <option value="sugestia">Sugestia</option>
                        <option value="prosba">Prośba</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="name">
                      Treść wiadomości</label>
                      <textarea name="message" id="message" class="form-control" rows="9" cols="25" required="required"
                        placeholder="Message"></textarea>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <button type="submit" class="btn btn-skin pull-right" id="btnContactUs">
                    Wyślij</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="widget-contact">
              <h5>Siedziba</h5>
              <address>
                <strong>Zakopane</strong><br>
                Zakopianka 60/1<br>
                San Francisco<br>
                <abbr title="Phone">Tel:</abbr> (77) 456-789-101
              </address>
              <address>
                <strong>Email</strong><br>
                <a href="mailto:#">contact@tobiass.com</a>
              </address>
            </div>
          </div>
        </div>
      </div>
    </section>

    <footer>
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-lg-12">
            <p>&copy;Copyright 2014 - Squad. All rights reserved.</p>
          </div>
        </div>
      </div>
    </footer>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/jquery.scrollTo.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/custom.js"></script>
  </body>
</html>