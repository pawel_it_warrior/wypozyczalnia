<?php
  require ("config.php");
?>
<!DOCTYPE html>
<html lang="pl">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Rejestracja</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="css/animate.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet">
    <link href="color/default.css" rel="stylesheet">
  </head>
  <body id="page-top" data-spy="scroll" data-target=".navbar-custom">
    <div id="preloader">
      <div id="load"></div>
    </div>
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header page-scroll">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
          <i class="fa fa-bars"></i>
          </button>
          <a class="navbar-brand" href="index.php">
            <h1>TOBI Ski</h1>
          </a>
        </div>
        <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
          <ul class="nav navbar-nav">
            <li><a href="index.php">Strona główna</a></li>
          </ul>
        </div>
      </div>
    </nav>
    <section id="rejestracja" class="home-section text-center">
      <div class="heading-rejestracja">
        <div class="container">
          <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
              <div class="wow bounceInDown" data-wow-delay="0.4s">
                <div class="section-heading">
                  <h2>Rejestracja</h2>
                  <i class="fa fa-2x fa-angle-down"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="container">
        <form id="rejestracja-form" class="form-horizontal" method="POST">
          <div class="form-group">
            <label class="col-md-4 control-label" for="imie" style="font-size: 80%;">Imię *</label>
            <div class="col-md-4">
              <input id="imie" name="imie" type="text" placeholder="Imię" class="form-control input-md" required="" >
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label" for="nazwisko" style="font-size: 80%;">Nazwisko *</label>
            <div class="col-md-4">
              <input id="nazwisko" name="nazwisko" type="text" placeholder="Nazwisko" class="form-control input-md" required="" >
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label" for="email" style="font-size: 80%;">Email *</label>
            <div class="col-md-4">
              <input id="email" name="email" type="text" placeholder="Email" class="form-control input-md" required="" >
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label" for="username" style="font-size: 80%;">Nazwa użytkownika *</label>
            <div class="col-md-4">
              <input id="username" name="username" type="text" placeholder="Nazwa użytkownika" class="form-control input-md" required="" >
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label" for="password" style="font-size: 80%;">Hasło *</label>
            <div class="col-md-4">
              <input id="password" name="password" type="password" placeholder="Hasło" class="form-control input-md" required="">
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label" for="cpassword" style="font-size: 80%;">Powtórz hasło *</label>
            <div class="col-md-4">
              <input id="cpassword" name="cpassword" type="password" placeholder="Hasło ponownie" class="form-control input-md" required="" >
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label" for="telefon" style="font-size: 80%;">Telefon</label>
            <div class="col-md-4">
              <input id="telefon" name="telefon" type="telefon" placeholder="Telefon" class="form-control input-md">
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label" for="agree">Wyrażam zgodę na przetwarzanie moich danych osobowych *</label>
            <div class="col-md-4"><br>
              <input id="agree" class="checkbox" type="checkbox" name="agree">
            </div>
          </div>
          <div class="form-group" >
            <div class="col-md-12" align="center">
              <input type="hidden" name="rejestracja" value="1">
              <input id="submitbutton" type="submit" class="btn btn-info" align="center" style="width: 30%;" value="Zarejestruj" />
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-12" align="center">
              <input type="reset" class="btn" align="center" style="width: 30%;" value="Anuluj" />
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-12" align="center">
              <span id="msg"></span>
            </div>
          </div>
        </form>
      </div>
    </section>
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-lg-12">
            <p>&copy;Copyright 2014 - Squad. All rights reserved.</p>
          </div>
        </div>
      </div>
    </footer>

    <script src="js/jquery.min.js"></script>
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/jquery.scrollTo.js"></script>
    <script src="js/rejestracja.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
    <script src="js/jquery.validate.js"></script>
  </body>
</html>