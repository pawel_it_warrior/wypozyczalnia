<?php
require ("config.php");

// REJESTRACJA
if (isset($_POST['rejestracja']) && !empty($_POST['rejestracja'])) {
  $json = '[{"success": "false"}]';
  if (!empty($_POST['imie']) && !empty($_POST['nazwisko']) && !empty($_POST['password']) && !empty($_POST['email'])) {
    $ready1 = false;
    $ready2 = false;
    $query = "SELECT 1 FROM users WHERE username = :username";
    $query_params = array(':username' => $_POST['username']);
    try {
      $stmt = $db->prepare($query);
      $result = $stmt->execute($query_params);
    }
    catch(PDOException $ex) {
      $json = '[{"success": "userexists"}]';
    }
    $row = $stmt->fetchAll();
    if (count($row) > 0) {
      $json = '[{"success": "userexists"}]';
    }
    else {
      $ready1 = true;
      $query = "SELECT 1 FROM users WHERE email = :email";
      $query_params = array(':email' => $_POST['email']);
      try {
        $stmt = $db->prepare($query);
        $result = $stmt->execute($query_params);
      }
      catch(PDOException $ex) {
        $json = '[{"success": "emailexists"}]';
      }
      $row = $stmt->fetchAll();
      if (count($row) > 0) {
        $json = '[{"success": "emailexists"}]';
      }
      else $ready2 = true;
    }
    if ($ready1 && $ready2) {
      $query = "INSERT INTO users (imie, nazwisko, username, password, salt, email, telefon, role) VALUES (:imie, :nazwisko, :username, :password, :salt, :email, :telefon, :role)";
      $salt = dechex(mt_rand(0, 2147483647)) . dechex(mt_rand(0, 2147483647));
      $password = hash('sha256', $_POST['password'] . $salt);
      for ($round = 0; $round < 65536; $round++) {
        $password = hash('sha256', $password . $salt);
      }
      $query_params = array(':imie' => $_POST['imie'], ':nazwisko' => $_POST['nazwisko'], ':username' => $_POST['username'], ':password' => $password, ':salt' => $salt, ':email' => $_POST['email'], ':telefon' => $_POST['telefon'], ':role' => 'customer');
      try {
        $stmt = $db->prepare($query);
        $result = $stmt->execute($query_params);
        $json = '[{"success": "true"}]';
      }
      catch(PDOException $ex) {
        $json = '[{"success": "false"}]';
      }
    }
  }
  else $json = '[{"success": "badinfo"}]';
  echo $json;
}




// REZERWACJA
if (isset($_POST['action']) && $_POST['action'] == 'pull') {
  $data_od = "'" . $_POST['data_od'] . "'"; // od kiedy chcemy wypożyczyć
  $data_do = "'" . $_POST['data_do'] . "'"; // do kiedy chcemy trzymać
  $data_od = str_replace("/", "-", $data_od);
  $data_do = str_replace("/", "-", $data_do);

  // wybiera wszystkie id rezerwacji gdzie wybrane daty mieszczą się w ich przedziale
  // czyli skladniki spod id zamowienia tej rezerwacji powinny byc niedostepne
  $query = "SELECT Rezerwacje.id AS RID, Rezerwacje.id_zamowienia AS R_IDZamowienia FROM Rezerwacje WHERE ($data_od >= Rezerwacje.data_od AND $data_do <= Rezerwacje.data_do)";
  $stmt = $db->prepare($query);
  $result = $stmt->execute();
  $items1 = Array();
  while ($r = $stmt->fetch()) {
    array_push($items1, $r['RID']);
  }

  // wybieram id tych skladnikow ktore naleza do wykluczonych zamowien
  $str = "'" . implode($items1, "', '") . "'";
  $q = "SELECT id_buty, id_deski, id_narty FROM Skladniki WHERE Skladniki.id_zamowienia IN ( $str )";
  $stmt = $db->prepare($q);
  $stmt->execute();

  $uid_buty = Array();
  $uid_deski = Array();
  $uid_narty = Array();
  while ($r = $stmt->fetch()) {
    if ($r['id_buty'] != NULL) {
      array_push($uid_buty, $r['id_buty']);
    }
    if ($r['id_deski'] != NULL) {
      array_push($uid_deski, $r['id_deski']);
    }
    if ($r['id_narty'] != NULL) {
      array_push($uid_narty, $r['id_narty']);
    }
  }

  $uid_buty = "'" . implode($uid_buty, "', '") . "'";
  $uid_deski = "'" . implode($uid_deski, "', '") . "'";
  $uid_narty = "'" . implode($uid_narty, "', '") . "'";

  $queryButy = "SELECT * FROM Buty
        INNER JOIN Buty_Producent   AS Producent  ON Buty.id_producent=Producent.id
        INNER JOIN Buty_Rozmiar   AS Rozmiar    ON Buty.id_rozmiar=Rozmiar.id
        INNER JOIN Buty_Plec    AS Plec     ON Buty.id_plec=Plec.id
        INNER JOIN Buty_Typ     AS Typ      ON Buty.id_typ=Typ.id
        INNER JOIN Buty_Zaawansowanie AS Zaawansowanie ON Buty.id_zaawansowanie=Zaawansowanie.id
        WHERE (Rozmiar.Buty_rozmiar=:rozmiar) AND (Buty.uid NOT IN ( $uid_buty )) AND Buty.wypozyczenie <> 1 ";
  $queryNarty = "SELECT * FROM Narty
        INNER JOIN Narty_Producent  AS Producent  ON Narty.id_producent=Producent.id
        INNER JOIN Narty_Rozmiar  AS Rozmiar    ON Narty.id_rozmiar=Rozmiar.id
        INNER JOIN Narty_Plec     AS Plec     ON Narty.id_plec=Plec.id
        INNER JOIN Narty_Typ    AS Typ      ON Narty.id_typ=Typ.id
        INNER JOIN Narty_Zaawansowanie AS Zaawansowanie ON Narty.id_zaawansowanie=Zaawansowanie.id
        WHERE (Rozmiar.Narty_rozmiar=:rozmiar) AND (Narty.uid NOT IN ( $uid_narty )) AND Narty.wypozyczenie <> 1 ";
  $queryDeski = "SELECT * FROM Deski
        INNER JOIN Deski_Producent  AS Producent  ON Deski.id_producent=Producent.id
        INNER JOIN Deski_Rozmiar  AS Rozmiar    ON Deski.id_rozmiar=Rozmiar.id
        INNER JOIN Deski_Plec     AS Plec     ON Deski.id_plec=Plec.id
        INNER JOIN Deski_Typ    AS Typ      ON Deski.id_typ=Typ.id
        INNER JOIN Deski_Zaawansowanie AS Zaawansowanie ON Deski.id_zaawansowanie=Zaawansowanie.id
        WHERE (Rozmiar.Deski_rozmiar=:rozmiar) AND (Deski.uid NOT IN ( $uid_deski )) AND Deski.wypozyczenie <> 1";
  $query_params = array(':rozmiar' => $_POST['rozmiar']);
  try {
    $stmtButy = $db->prepare($queryButy);
    $result = $stmtButy->execute($query_params);
    $stmtNarty = $db->prepare($queryNarty);
    $result = $stmtNarty->execute($query_params);
    $stmtDeski = $db->prepare($queryDeski);
    $result = $stmtDeski->execute($query_params);
  }
  catch(PDOException $ex) {
    die("Failed to run query: " . $ex->getMessage());
  }

  $items = Array();
  while ($r = $stmtNarty->fetch()) {
    $r["rodzaj"] = "narty";
    $r["rozmiar"] = $_POST['rozmiar'];
    array_push($items, $r);
  }
  while ($r = $stmtButy->fetch()) {
    $r["rodzaj"] = "buty";
    $r["rozmiar"] = $_POST['rozmiar'];
    array_push($items, $r);
  }
  while ($r = $stmtDeski->fetch()) {
    $r["rodzaj"] = "deski";
    $r["rozmiar"] = $_POST['rozmiar'];
    array_push($items, $r);
  }
  echo json_encode($items, JSON_UNESCAPED_UNICODE);
}

if (isset($_POST['action']) && $_POST['action'] == 'koszyk') {
  $kosz = $_POST['zamowienie'];
  if (!isset($_SESSION['koszyk'])) {
    $_SESSION['koszyk'] = array();
  }
  foreach ($kosz as $it) {
    array_push($_SESSION['koszyk'], $it);
  }
  $data_od = str_replace("/", "-", $_POST['data_od']);
  $data_do = str_replace("/", "-", $_POST['data_do']);
  $_SESSION['koszyk']['data_od'] = $data_od;
  $_SESSION['koszyk']['data_do'] = $data_do;
}

function random_string($length, $ranges = array('0-9', 'A-Z')) {
  foreach ($ranges as $r) $s.= implode(range(array_shift($r = explode('-', $r)), $r[1]));
  while (strlen($s) < $length) $s.= $s;
  return substr(str_shuffle($s), 0, $length);
}

if (isset($_POST['rezerwacja']) && !empty($_POST['rezerwacja'])) {
  $json = '[{"success": "false"}]';
  $emailexists = false;

  if (!empty($_POST['imie']) && !empty($_POST['nazwisko']) && !empty($_POST['email']))
  {
      $query = "SELECT 1 FROM users WHERE email = :email";
      $query_params = array(':email' => $_POST['email']);
      try {
        $stmt = $db->prepare($query);
        $result = $stmt->execute($query_params);
      }
      catch(PDOException $ex) {
        $emailexists = true;
      }

      if (!$emailexists) {
          $query = "INSERT INTO users (imie, nazwisko, email, telefon, role) VALUES (:imie, :nazwisko, :email, :telefon, :role)";
          $query_params = array(':imie' => $_POST['imie'], ':nazwisko' => $_POST['nazwisko'], ':email' => $_POST['email'], ':telefon' => $_POST['telefon'], ':role' => 'nobody');
          try {
            $stmt = $db->prepare($query);
            $result = $stmt->execute($query_params);
            $json = '[{"success": "true"}]';
          }
          catch(PDOException $ex) {
            $json = '[{"success": "false"}]';
          }
      }

    $query = "SELECT id FROM users WHERE email = :email LIMIT 1";
    $query_params = array(':email' => $_POST['email']);
    $stmt = $db->prepare($query);
    $stmt->execute($query_params);
    $r = $stmt->fetch();
    $id_usera = $r['id']; // id usera

    $query = "INSERT INTO Zamowienia (id_customer) VALUES (:id_customer)";
    $query_params = array(':id_customer' => $id_usera);
    $stmt = $db->prepare($query);
    $stmt->execute($query_params);
    $query = "SELECT max(id_zamowienia) AS last FROM Zamowienia";
    $stmt = $db->prepare($query);
    $stmt->execute();
    $r = $stmt->fetch();
    $last_id_zamowienia = $r['last'];

    foreach ($_SESSION['koszyk'] as $k) {
      if ($k[0] == "narty") {
        $query = "INSERT INTO Skladniki (id_zamowienia, id_narty) VALUES (:id_zamowienia, :id_narty)";
        $query_params = array(':id_zamowienia' => $last_id_zamowienia, ':id_narty' => $k[7]);
        $stmt = $db->prepare($query);
        $stmt->execute($query_params);
      }
      if ($k[0] == "buty") {
        $query = "INSERT INTO Skladniki (id_zamowienia, id_buty) VALUES (:id_zamowienia, :id_buty)";
        $query_params = array(':id_zamowienia' => $last_id_zamowienia, ':id_buty' => $k[7]);
        $stmt = $db->prepare($query);
        $stmt->execute($query_params);
      }
      if ($k[0] == "deski") {
        $query = "INSERT INTO Skladniki (id_zamowienia, id_deski) VALUES (:id_zamowienia, :id_deski)";
        $query_params = array(':id_zamowienia' => $last_id_zamowienia, ':id_deski' => $k[7]);
        $stmt = $db->prepare($query);
        $stmt->execute($query_params);
      }
    }

    $nr_rez = random_string(6);
    $query = "INSERT INTO Rezerwacje (id_zamowienia, data_od, data_do, numer_rezerwacji) VALUES (:id_zamowienia, :data_od, :data_do, :numer)";
    $query_params = array(':id_zamowienia' => $last_id_zamowienia, ':data_od' => $_SESSION['koszyk']['data_od'], ':data_do' => $_SESSION['koszyk']['data_do'], ':numer' => $nr_rez);
    $stmt = $db->prepare($query);
    $stmt->execute($query_params);

    $json = '[{"success": "true"}, {"kod": "' . $nr_rez . '"}]';
    }
  echo $json;
}
?>