$(document).ready(function() {

  var wg = 0;
  var wz = 0;
  var rb = 0;
  var t_od = 0;
  var t_do = 0;

  function pulldata(roz, t_od, t_do) {
    $.ajax({
      type: "POST",
      url: "dbactions.php",
      data: {
        action: "pull",
        rozmiar: roz,
        data_od: t_od,
        data_do: t_do
      },
      dataType: 'text',
      success: function(data) {
        var response = $.parseJSON(data);
        $('#items').empty();
        for (var i = 0; i < response.length - 1; i++) {
          var str = '<tr id="' + i + '"><td>' + response[i].rodzaj + '</td>';
          if (response[i].rodzaj == "buty") {
            str += '<td>' + response[i].buty_model + '</td>';
            str += '<td>' + response[i].buty_producent_nazwa + '</td>';
            str += '<td>' + response[i].buty_typ_nazwa + '</td>';
            str += '<td>' + response[i].buty_zaawansowanie_nazwa + '</td>';
            str += '<td>' + response[i].buty_plec_nazwa + '</td>';
            str += '<td style="display: none">' + response[i].rozmiar + '</td>';
            str += '<td style="display: none">' + response[i].uid + '</td>';
            str += '<td><button value="ok_' + i + '"><span class="glyphicon glyphicon-ok"></span></button><button value="no_' + i + '"><span class="glyphicon glyphicon-remove"></span></button></td></tr>';
          }
          if (response[i].rodzaj == "narty") {
            str += '<td>' + response[i].narty_model + '</td>';
            str += '<td>' + response[i].narty_producent_nazwa + '</td>';
            str += '<td>' + response[i].narty_typ_nazwa + '</td>';
            str += '<td>' + response[i].narty_zaawansowanie_nazwa + '</td>';
            str += '<td>' + response[i].narty_plec_nazwa + '</td>';
            str += '<td style="display: none">' + response[i].rozmiar + '</td>';
            str += '<td style="display: none">' + response[i].uid + '</td>';
            str += '<td><button value="ok_' + i + '"><span class="glyphicon glyphicon-ok"></span></button><button value="no_' + i + '"><span class="glyphicon glyphicon-remove"></span></button></td></tr>';
          }
          if (response[i].rodzaj == "deski") {
            str += '<td>' + response[i].deski_model + '</td>';
            str += '<td>' + response[i].deski_producent_nazwa + '</td>';
            str += '<td>' + response[i].deski_typ_nazwa + '</td>';
            str += '<td>' + response[i].deski_zaawansowanie_nazwa + '</td>';
            str += '<td>' + response[i].deski_plec_nazwa + '</td>';
            str += '<td style="display: none">' + response[i].rozmiar + '</td>';
            str += '<td style="display: none">' + response[i].uid + '</td>';
            str += '<td><button value="ok_' + i + '"><span class="glyphicon glyphicon-ok"></span></button><button value="no_' + i + '"><span class="glyphicon glyphicon-remove"></span></button></td></tr>';
          }
          $('#items').append(str);
        };
        $('#paginacja').remove();
        $("#tabela").simplePagination();
      }
    });
  }

  $('#formularz_rezerwacji').validate({
  rules: {
    imie: {
      required: true,
    },
    nazwisko: {
      required: true,
    },
    email: {
      required: true,
      email: true,
    },
    telefon: {
      minlength: 6,
    },
    agree: "required",
  },
  highlight: function(element) {
    $(element).closest('.control-group').removeClass('success').addClass('error');
  },
  success: function(element) {
    element
    .text('OK!').addClass('valid')
    .closest('.control-group').removeClass('error').addClass('success');
  }
  });

  $(function() {
    $("#rezerwuj_submit").click(function(event) {
      event.preventDefault();
      var a=$('#imie').val();
      var b=$('#nazwisko').val();
      var c=$('#email').val();
      var d=$('#agree').is(":checked");
      // var hid=$('#zalogowany_user').val();
      if ( a != " " && b != " " && c != " " && d == true ){

        var post_data = $('#formularz_rezerwacji').serialize();
        $.post('dbactions.php', post_data, function(data) {
          success: {
            console.log(data);
            var response = $.parseJSON(data);
            if(response[0].success == "true"){
              $('#rezerwuj_submit').prop('disabled', true);
              $('#msg').html('<font size="150%">DOKONANO REZERWACJI</font><br><font size="110%">Twój unikatowy numer zamówienia: <span style="color: red; font-weight:bold;">' + response[1].kod + '</span></font><br><br><font size="30%">Prosimy o zapisanie powyższego kodu i przedstawienie go w celu odbioru rezerwacji.</font>');
            }
          }
        });
      }
    });
  });

  $('#waga').change(function() {
      if ($('#waga').val() != "") wg = 1;
      else wg = 0;
      if (wg == 1 && wz == 1 && rb == 1 && t_od == 1 && t_do == 1) pulldata($('#rozmiarnogi').val(), $('#data_od').val(), $('#data_do').val());
  });

  $('#wzrost').change(function() {
      if ($('#wzrost').val() != "") wz = 1;
      else wz = 0;
      if (wg == 1 && wz == 1 && rb == 1 && t_od == 1 && t_do == 1) pulldata($('#rozmiarnogi').val(), $('#data_od').val(), $('#data_do').val());
  });

  $('#rozmiarnogi').change(function() {
      if ($('#rozmiarnogi').val() != "") rb = 1;
      else rb = 0;
      if (wg == 1 && wz == 1 && rb == 1 && t_od == 1 && t_do == 1) pulldata($('#rozmiarnogi').val(), $('#data_od').val(), $('#data_do').val());
  });

  $('#data_od').change(function() {
      if ($('#data_od').val() != "") t_od = 1;
      else t_od = 0;
      if (wg == 1 && wz == 1 && rb == 1 && t_od == 1 && t_do == 1) pulldata($('#rozmiarnogi').val(), $('#data_od').val(), $('#data_do').val());
  });

  $('#data_do').change(function() {
      if ($('#data_do').val() != "") t_do = 1;
      else t_do = 0;
      if (wg == 1 && wz == 1 && rb == 1 && t_od == 1 && t_do == 1) pulldata($('#rozmiarnogi').val(), $('#data_od').val(), $('#data_do').val());
  });

  $('#filter').change(function() {
      var rex = new RegExp($(this).val(), 'i');
      $('.searchable tr').hide();
      $('.searchable tr').filter(function() {
          return rex.test($(this).text());
      }).show();
  });

  $('#anuluj').click(function(){
      window.location.replace("index.php");
  });
});