$(document).ready(function(){
	$('#rejestracja-form').validate({
    rules: {
	    imie: {
        required: true,
	    },
	    nazwisko: {
	    	required: true,
	    },
			username: {
        required: true,
        minlength: 5,
	    },
		  password: {
				required: true,
				minlength: 5,
			},
			cpassword: {
				required: true,
				minlength: 5,
				equalTo: "#password",
			},
      email: {
        required: true,
        email: true,
      },
	   	telefon: {
      	minlength: 6,
      },
	  	agree: "required",
    },
		highlight: function(element) {
			$(element).closest('.control-group').removeClass('success').addClass('error');
		},
		success: function(element) {
			element
			.text('OK!').addClass('valid')
			.closest('.control-group').removeClass('error').addClass('success');
		}
  });
  $(function() {
    $("#submitbutton").click(function(event) {
  		event.preventDefault();
    	var a=$('#imie').val();
			var b=$('#nazwisko').val();
			var c=$('#username').val();
			var d=$('#password').val();
			var e=$('#cpassword').val();
			var f=$('#email').val();
			var g=$('#agree').is(":checked");
			if (a != " " && b != " " && c != " " && d != " " && e != " " && f != " " && d == e && g == true){
				var post_data = $('#rejestracja-form').serialize();
	  		$.post('dbactions.php', post_data, function(data) {
	  			success: {
	  				var response = $.parseJSON(data);
	  				if(response[0].success == "true")
	  				{
	  					$('#submitbutton').prop('disabled', true);
	  					$('#msg').html('<font size="150%">ZAREJESTROWANO</font>');
	  					function gothere(){
	  						document.location.href = 'index.php';
	  					}
	  					setTimeout(gothere, 1500);
	  				} else if (response[0].success == "userexists" ){
	  					$('#msg').html('Nazwa użytkownika jest zajęta');
	  				} else if (response[0].success == "emailexists" ){
							$('#msg').html('Istnieje już użytkownik o tym adresie email');
	  				} else if (response[0].success == "badinfo" ){
	  					$('#msg').html('Podano niepoprawne dane');
	  				}	else if (response[0].success == "false" ){
	  					$('#msg').html('Wpisz poprawne dane');
	  				}
	  			}
	      });
  		}
  	});
  });
});