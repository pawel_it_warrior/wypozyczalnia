<?php
// ini_set( 'display_errors', 'On' );
// error_reporting( E_ALL );
require ("config.php");

$submitted_username = '';
if (isset($_POST['login'])) {
  $query = "SELECT * FROM users WHERE username = :username LIMIT 1";
  $query_params = array(':username' => $_POST['username']);
  try {
    $stmt = $db->prepare($query);
    $result = $stmt->execute($query_params);
  } catch(PDOException $ex) {
    die("Failed to run query: " . $ex->getMessage());
  }
  $row = $stmt->fetch();
  if ($row) {
    $check_password = hash('sha256', $_POST['password'] . $row['salt']);
    for ($round = 0; $round < 65536; $round++) {
      $check_password = hash('sha256', $check_password . $row['salt']);
    }
    if ($check_password === $row['password']) {
      $_SESSION['logged'] = true;
    } else {
      $_SESSION['logged'] = false;
    }
  }
  if ($_SESSION['logged']) {
    unset($row['salt']);
    unset($row['password']);
    $_SESSION['user'] = $row;
  } else {
    print ("Login Failed.");
    $submitted_username = htmlentities($_POST['username'], ENT_QUOTES, 'UTF-8');
  }
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Wypożyczalnia</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="css/animate.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet">
    <link href="color/default.css" rel="stylesheet">
    <!-- Tutaj lepiej żeby skrypty były na początku ładowania strony -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/jquery.scrollTo.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/jquery.datetimepicker.js"></script>
    <script>
    $(function() {
      $( "#logowanie" ).click(function(event) {
        event.preventDefault();
        if ( $("#username").val() && $("#password").val() ){
          $('#zaloguj').submit();
        }
      });
    });
    </script>
  </head>
  <body id="page-top" data-spy="scroll" data-target=".navbar-custom">
    <div id="preloader">
      <div id="load"></div>
    </div>
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header page-scroll">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
          <i class="fa fa-bars"></i>
          </button>
          <a class="navbar-brand" href="index.php">
            <h1>TOBIASS</h1>
          </a>
        </div>
        <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
          <ul class="nav navbar-nav">
            <li><a href="#indeks">Oferta</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Śledź nas<b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="facebook.com">Facebook</a></li>
                <li><a href="twitter.com">Twitter</a></li>
              </ul>
            </li>
            <?php
            if ($_SESSION['logged']){
              echo "<li><a href=\"logout.php\">Wyloguj</a></li>";
            } else {
              echo '
              <form id="zaloguj" class="navbar-form navbar-right" style="margin-right: 5px;" method="POST" action="index.php">
                <div class="form-group">
                  <input type="text" class="form-control" id="username" name="username" placeholder="Username">
                </div>
                <div class="form-group">
                  <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                </div>
                <input type="hidden" name="login" value="1" />
                <button type="submit" class="btn btn-default" id="logowanie" name="logowanie">Zaloguj</button>
              </form>
              '; } ?>
          </ul>
        </div>
      </div>
    </nav>
    <section id="intro" class="intro">
      <div class="wow bounceInDown slogan" data-wow-delay="0.1s" >
        <h2>Wypożyczalnia <span class="text_color">TOBIASS</span> </h2>
        <h4>Zarezerwuj swój sprzęt narciarski już dziś</h4>
      </div>
      <div class="page-scroll">
        <a href="#indeks" class="btn btn-circle">
        <i class="fa fa-angle-double-down animated"></i>
        </a>
      </div>
    </section>
    <section id="indeks" class="home-section text-center">
      <div class="heading-indeks">
        <div class="container">
          <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
              <div class="wow bounceInDown" data-wow-delay="0.4s">
                <div class="section-heading">
                  <h2>Nasza oferta</h2>
                  <i class="fa fa-2x fa-angle-down"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-lg-2 col-lg-offset-5">
            <hr class="marginbot-50">
          </div>
        </div>
        <div class="row" <?php if ($_SESSION['logged']) echo "align=\"center\""; ?> >
          <a href="rezerwacja.php" style="color:#000;">
          <?php if (isset($_SESSION['logged']) && $_SESSION['logged']) echo "<div class=\"col-xs-6 col-sm-3 col-md-4\">";
            else echo "<div class=\"col-xs-6 col-sm-3 col-md-3\">"; ?>
              <div class="wow bounce" data-wow-delay="0.2s">
                <div class="team boxed-grey bounce">
                  <div class="inner">
                    <h5>Rezerwacja</h5>
                    <p class="subtitle">Wybierz sprzęt</p>
                    <div class="avatar"><img src="img/team/1.png" alt="" class="img-responsive img-circle" /></div>
                  </div>
                </div>
              </div>
            </div>
          </a>
          <a href="about.php" style="color:#000;">
          <?php if (isset($_SESSION['logged']) && $_SESSION['logged']) echo "<div class=\"col-xs-6 col-sm-3 col-md-4\">";
            else echo "<div class=\"col-xs-6 col-sm-3 col-md-3\">"; ?>
              <div class="wow bounce" data-wow-delay="0.5s">
                <div class="team boxed-grey bounce">
                  <div class="inner">
                    <h5>O firmie</h5>
                    <p class="subtitle">Szczegóły</p>
                    <div class="avatar"><img src="img/team/2.png" alt="" class="img-responsive img-circle" /></div>
                  </div>
                </div>
              </div>
            </div>
          </a>
          <a href="contact.php" style="color:#000;">
          <?php if (isset($_SESSION['logged']) && $_SESSION['logged']) echo "<div class=\"col-xs-6 col-sm-3 col-md-4\">";
            else echo "<div class=\"col-xs-6 col-sm-3 col-md-3\">"; ?>
              <div class="wow bounce" data-wow-delay="0.8s">
                <div class="team boxed-grey bounce">
                  <div class="inner">
                    <h5>Kontact</h5>
                    <p class="subtitle">Napisz do nas</p>
                    <div class="avatar"><img src="img/team/3.png" alt="" class="img-responsive img-circle" /></div>
                  </div>
                </div>
              </div>
            </div>
          </a>
          <?php
          if (!$_SESSION['logged']) echo '
          <a href="rejestracja.php" style="color:#000;">
            <div class="col-xs-6 col-sm-3 col-md-3">
              <div class="wow bounce" data-wow-delay="1s">
                <div class="team boxed-grey bounce">
                  <div class="inner">
                    <h5>Rejestracja</h5>
                    <p class="subtitle">Zostań klientem</p>
                    <div class="avatar"><img src="img/team/4.png" alt="" class="img-responsive img-circle" /></div>
                  </div>
                </div>
              </div>
            </div>
          </a>'; ?>
        </div>
      </div>
    </section>

    <footer>
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-lg-12">
            <p>&copy;Copyright 2015 - TOBISKI. Wszelkie prawa zastrzeżone.</p>
            <p><a href="pa/login.php" style="font-weight: bold; color: #fff;">Panel administracyjny</a></p>
          </div>
        </div>
      </div>
    </footer>
    <script src="js/custom.js"></script>
  </body>
</html>